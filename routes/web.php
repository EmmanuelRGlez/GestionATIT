<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/periodos', ['uses' => 'PeriodoControlador@index', 'as' => 'periodo.index']);
Route::get('/periodos/registro', ['uses' => 'PeriodoControlador@create', 'as' => 'periodo.create']);
Route::post('/periodos/registro', ['uses' => 'PeriodoControlador@store', 'as' => 'periodo.store']);
Route::get('/periodos/{id}/detalles', ['uses' => 'PeriodoControlador@show', 'as' => 'periodo.detalles']);
Route::get('/periodos/{id}/editar', ['uses' => 'PeriodoControlador@edit', 'as' => 'periodo.editar']);
Route::put('/periodos/{id}/update', ['uses' => 'PeriodoControlador@update', 'as' => 'periodo.update']);
Route::delete('/periodos/{id}/eliminar', ['uses' => 'PeriodoControlador@destroy', 'as' => 'periodo.eliminar']);

Route::get('/personal', ['uses' => 'PersonalControlador@index', 'as' => 'personal.index']);
Route::get('/personal/registro', ['uses' => 'PersonalControlador@create', 'as' => 'personal.create']);
Route::post('/personal/registro', ['uses' => 'PersonalControlador@store', 'as' => 'personal.store']);
Route::get('/personal/{id}/detalles', ['uses' => 'PersonalControlador@show', 'as' => 'personal.detalles']);
Route::get('/personal/{id}/editar', ['uses' => 'PersonalControlador@edit', 'as' => 'personal.editar']);
Route::put('/personal/{id}/update', ['uses' => 'PersonalControlador@update', 'as' => 'personal.update']);
Route::delete('/personal/{id}/eliminar', ['uses' => 'PersonalControlador@destroy', 'as' => 'personal.eliminar']);

Route::get('/encuestas/responder/{token}', ['uses' => 'HorarioControlador@create', 'as' => 'encuesta.create']);

Route::get('/horario', ['uses' => 'HorarioControlador@index', 'as' => 'horario.index'])->middleware('auth');
Route::post('/horario/envio/todos', ['uses' => 'HorarioControlador@envioTodos', 'as' => 'horario.envioTodos'])->middleware('auth');;
Route::post('/horario/envio/academia', ['uses' => 'HorarioControlador@envioAcademia', 'as' => 'horario.envioAcademia'])->middleware('auth');
Route::post('/horario/{id}/reenviar', ['uses' => 'HorarioControlador@reenviar', 'as' => 'horario.reenviar'])->middleware('auth');
Route::post('/horario/{token}/registro', ['uses' => 'HorarioControlador@store', 'as' => 'horario.store'])->middleware('auth');
Route::get('/respuesta/{id}/detalles', ['uses' => 'HorarioControlador@show', 'as' => 'horario.show'])->middleware('auth');

Route::get('/academias/{id}', ['uses' => 'HomeControlador@academias', 'as' => 'academias.academias']);

Route::get('/materias', ['uses' => 'MateriaControlador@create', 'as' => 'materias.create'])->middleware('auth');;
Route::post('/materias/registro', ['uses' => 'MateriaControlador@store', 'as' => 'materias.store'])->middleware('auth');;

Route::get('/estadisticas', ['uses' => 'EstadisticasControlador@index', 'as' => 'estadisticas.index'])->middleware('auth');;
Route::get('/estadisticas/{id}/respuestas', ['uses' => 'EstadisticasControlador@respuestas', 'as' => 'estadisticas.respuestas'])->middleware('auth');;
Route::get('/estadisticas/{id}/graficas', ['uses' => 'EstadisticasControlador@graficas', 'as' => 'estadisticas.graficas'])->middleware('auth');;

/* Index */
Route::get('/', ['uses' => 'HomeControlador@index', 'as' => 'home.login']);
Route::post('/', ['uses' => 'Auth\LoginController@login', 'as' => 'login']);
Route::get('logout', ['uses' => 'Auth\LoginController@logout', 'as' => 'login.logout']);

Route::get('/crear', function(){
	// App\User::create([
	// 	'email' => 'ordaz@upslp.edu.mx',
	// 	'password' => bcrypt(130558),
	// ]);
	return bcrypt('HOLA130558');
});
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMateriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('semestre');
            $table->text('nombre');
            // $table->text('seriacion_id');
            $table->integer('creditos');
            $table->string('clave', 5);
            $table->integer('horas');
            $table->integer('academia_id')->unsigned();
            $table->integer('carrera_id')->unsigned();

            $table->softDeletes();
            $table->timestamps();
            
            $table->foreign('academia_id')->references('id')->on('academias');
            $table->foreign('carrera_id')->references('id')->on('carreras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materias');
    }
}

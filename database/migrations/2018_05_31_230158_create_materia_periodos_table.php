<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMateriaPeriodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materia_periodos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('periodo_id')->unsigned();
            $table->integer('materia_id')->unsigned();
            $table->integer('optativa_id')->unsigned()->nullable();

            $table->foreign('periodo_id')->references('id')->on('periodos');
            $table->foreign('materia_id')->references('id')->on('materias');
            $table->foreign('optativa_id')->references('id')->on('materia_optativas');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materia_periodos');
    }
}

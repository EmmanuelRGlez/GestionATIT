<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspecialidadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('especialidads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('personal_id')->unsigned();
            $table->integer('periodo_id')->unsigned();
            $table->string('licenciatura', 80);
            $table->string('maestria', 80);
            $table->integer('experiencia_docente');
            $table->integer('experiencia_profesional');
            
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('personal_id')->references('id')->on('personals');
            $table->foreign('periodo_id')->references('id')->on('periodos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('especialidads');
    }
}

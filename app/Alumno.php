<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alumno extends Model
{
	use SoftDeletes;
    protected $table = 'alumnos';
	protected $fillable = ['nombre', 'apellido_paterno', 'apellido_materno', 'carrera_id'];
	public $timestamps = true;

	public function carrera()
	{
		return $this->belongsTo('App\Carrera');
	}
}
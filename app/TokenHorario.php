<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TokenHorario extends Model
{
	use SoftDeletes;

    protected $table = 'token_horarios';
	protected $fillable = ['personal_id', 'periodo_id', 'token', 'updated_at'];
	public $timestamps = true;
	protected $dates = ['deleted_at'];

	public function personal()
	{
		return $this->belongsTo('App\Personal');
	}

	public function periodo()
	{
		return $this->belongsTo('App\Periodo');
	}
}

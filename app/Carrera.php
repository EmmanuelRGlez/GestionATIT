<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Carrera extends Model
{
	use SoftDeletes;
    protected $table = 'carreras';
	protected $fillable = ['nombre', 'nombre_min'];
	public $timestamps = false;

	public function materias()
	{
		return $this->hasMany('App\Alumno', 'carrera_id', 'id');
	}
}

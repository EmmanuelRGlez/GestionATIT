<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Especialidad extends Model
{
	use SoftDeletes;
	protected $table = 'especialidads';
	protected $fillable = ['personal_id', 'periodo_id', 'licenciatura', 'maestria', 'experiencia_docente', 'experiencia_profesional'];
	protected $dates = ['deleted_at'];
	public $timestamps = true;

	public function profesor()
	{
		return $this->belongsTo('App\Personal');
	}

	public function periodo()
	{
		return $this->belongsTo('App\Periodo');
	}
}

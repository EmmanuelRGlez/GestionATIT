<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Materia extends Model
{
	use SoftDeletes;
    protected $table = 'materias';
	protected $fillable = ['semestre', 'nombre', 'creditos', 'clave', 'horas', 'academia_id'];
	protected $dates = ['deleted_at'];
	public $timestamps = true;

	public function academia()
	{
		return $this->belongsTo('App\Academia');
	}

	public function carrera()
	{
		return $this->belongsTo('App\Carrera');
	}

	public function optativas()
	{
		return $this->hasMany('App\MateriaOptativa', 'materia_id', 'id');
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Academia extends Model
{
    protected $table = 'academias';
	protected $fillable = ['nombre'];
	public $timestamps = true;

	public function materias()
	{
		return $this->hasMany('App\Materia', 'academia_id', 'id');
	}

	public function profesores()
	{
		return $this->hasMany('App\Personal', 'academia_id', 'id');
	}
}

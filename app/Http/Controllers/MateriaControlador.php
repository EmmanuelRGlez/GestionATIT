<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Periodo;
use App\Materia;
use App\MateriaPeriodo;

class MateriaControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $periodos = Periodo::pluck('nombre', 'id');
        $materias = Materia::all();
        return view('materias.registro', compact('periodos', 'materias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $periodo_id = $request->periodo_id;
        if(count($request->materia_id) > 0){
            foreach ($request->materia_id as $materia_id) {
                MateriaPeriodo::create(compact('periodo_id', 'materia_id'));
            }

        }
        
        if(count($request->optativas) > 0){
            foreach ($request->optativas as $materia) {
                MateriaPeriodo::create(['periodo_id' => $periodo_id, 'materia_id' => $materia['materia_id'], 'optativa_id' => $materia['optativa_id']]);
            }
        }

        return response()->json('Exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

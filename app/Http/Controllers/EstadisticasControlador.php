<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Periodo;
use App\TokenHorario;

class EstadisticasControlador extends Controller
{

    // function __construct(){
    //     $this->middleware(['auth', 'roles'], ['except' => ['store', 'registroExterno']]);
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $encuestas = TokenHorario::withTrashed()->get();
        $periodos = Periodo::pluck('nombre', 'id');
        return view('estadisticas.estadisticas', compact('encuestas', 'periodos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function respuestas($id)
    {
        $encuestas = TokenHorario::withTrashed()->where('periodo_id', $id)->get();
        if($encuestas->count() > 0){
            foreach ($encuestas as $encuesta) {
                if($encuesta->deleted_at == null){
                    $status = "No";
                    $respuesta = '<i class="red-text material-icons">remove</i>';
                }else{
                    $status = "Sí";
                    $respuesta = $encuesta->deleted_at->format('d/m/Y');
                }

                $respuestas[] = [
                    "id" => $encuesta->id,
                    "nombre" => $encuesta->personal->nombre." ".$encuesta->personal->apellido_paterno." ".$encuesta->personal->apellido_materno,
                    "envio" => $encuesta->created_at->format('d/m/Y'), 
                    "respuesta" => $respuesta,
                    "status" => $status];
            }
        }else $respuestas = "null";

        return response()->json($respuestas);
    }

    public function graficas($id)
    {
        $si = TokenHorario::withTrashed()->where('periodo_id', $id)->whereNotNull('deleted_at')->count();
        $no = TokenHorario::where('periodo_id', $id)->count();
        $type = 'pie'; $labels = ['Sí', 'No']; $backgroundColor = ['#e77817', '#1E88E5']; $data = [$si, $no];
        
        return response()->json(compact('type', 'labels', 'data', 'backgroundColor'));
    }
}

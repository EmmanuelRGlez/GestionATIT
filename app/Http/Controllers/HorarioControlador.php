<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Academia;
use App\Materia;
use App\Periodo;
use App\MateriaPeriodo;
use App\Personal;
use App\TokenHorario;
use App\HorarioPersonal;
use App\MateriaPersonal;
use App\Especialidad;

class HorarioControlador extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $academias = Academia::pluck('nombre', 'id');
        $periodos = Periodo::pluck('nombre', 'id');
        return view('horario.compartir', compact('academias', 'periodos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($token)
    {
        $token_h = TokenHorario::where('token', $token)->get()->first();
        if($token_h != null){
            $academias = Academia::pluck('nombre', 'id');
            $personal = $token_h->personal;
            $periodo = $token_h->periodo;
            $materias = collect(MateriaPeriodo::where('periodo_id', $periodo->id)->get());
            $especialidad = Especialidad::where('personal_id', $personal->id)->first();

            return view('horario.registro', compact('academias', 'personal', 'periodo', 'materias', 'token', 'especialidad'));
        }else{
            return view('error.404');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $token)
    {
        $dias = ['lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado'];
        $semana = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
        $token_h = TokenHorario::where('token', $token)->get()->first();
        $personal = $token_h->personal;
        $periodo = $token_h->periodo;
        $ok = false;
        $lastRecord = TokenHorario::withTrashed()->where('personal_id', $personal->id)->where('periodo_id', $periodo->id)->oldest()->first();
        if($lastRecord != null){
            if($lastRecord->token != $token){
                $lastRecord->forceDelete();
                \DB::table('horario_personals')->where('personal_id', $personal->id)->where('periodo_id', $periodo->id)->delete();
                \DB::table('materia_personals')->where('personal_id', $personal->id)->where('periodo_id', $periodo->id)->delete();
            }
        }

        //  Guardar datos de experiencia
        Especialidad::create([
            'personal_id'   => $personal->id, 
            'periodo_id'    => $periodo->id, 
            'licenciatura'  => $request->licenciatura, 
            'maestria'      => $request->maestria, 
            'experiencia_docente'       => $request->experiencia_docente, 
            'experiencia_profesional'   => $request->experiencia_profesional
        ]);
        //  Guardar datos del usuario
        $personal->fill($request->all());
        $personal->save();

        //  Guardar horarios
        foreach ($dias as $dia) {
            if($request->$dia != null){
                foreach ($request->$dia as $horario) {
                    if(HorarioPersonal::create(['personal_id' => $personal->id, 'periodo_id' => $periodo->id, 'dia' => $dia, 'horario' => $horario]))
                        $ok = true;
                    else 
                        $ok = false;
                }
            }
        }

        //  Guardar Materias
        if(count($request->materia_id) > 0){
            foreach ($request->materia_id as $materia_id) {
                if(MateriaPersonal::create(['personal_id' => $personal->id, 'periodo_id' => $periodo->id, 'materia_id' => $materia_id]))
                    $ok = true;
                else 
                    $ok = false;
            }

        }
        
        if(count($request->optativas) > 0){
            foreach ($request->optativas as $materia) {
                if(MateriaPersonal::create(['personal_id' => $personal->id, 'periodo_id' => $periodo->id, 'materia_id' => $materia['materia_id'], 'optativa_id' => $materia['optativa_id']]))
                    $ok = true;
                else 
                    $ok = false;

            }
        }

        if($ok){
            $token_h->delete();
            return response()->json('Exito');
        }else
            return response()->json('Error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $encuesta = TokenHorario::withTrashed()->where('id', $id)->first();
        $materias = MateriaPersonal::where('personal_id', $encuesta->personal_id)
                                ->where('periodo_id', $encuesta->periodo_id)->get();
        $horarios = HorarioPersonal::where('personal_id', $encuesta->personal_id)
                                ->where('periodo_id', $encuesta->periodo_id)->get();
        $especialidad = Especialidad::where('personal_id', $encuesta->personal_id)
                                ->where('periodo_id', $encuesta->periodo_id)->first();;
        return view('horario.detalle', ['personal' => $encuesta->personal, 'materias' => $materias, 'horarios' => $horarios, 'especialidad' => $especialidad]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function correo(Personal $personal, $periodo_id)
    { 
        $personal_id = $personal->id;
        $periodo = Periodo::find($periodo_id);
        if($personal->token != null){
            $token = $personal->token->token;
        }else{
            $token = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 25);
            TokenHorario::create(compact('periodo_id', 'personal_id', 'token'));
        }
        Mail::send('email.templateForm', ['token' => $token, 'periodo' => $periodo], function($m) use ($personal){
            $m->to($personal->email, $personal->nombre.' '.$personal->apellido_paterno.' '.$personal->apellido_materno)->subject('Encuesta de disponibilidad de horario');
        });
    }

    public function envioTodos(Request $request)
    {
        $maestros = Personal::all();
        if ($maestros->count() > 0) {
            foreach ($maestros as $maestro) {
                if($maestro->email != null){
                    $this->correo($maestro, $request->periodo_id);
                }
            }
            return response()->json('Exito');
        }else
            return response()->json('Error');
    }

    public function envioAcademia(Request $request)
    {
        $maestros = Personal::where('academia_id', $request->academia_id)->get();
        if ($maestros->count() > 0) {
            foreach ($maestros as $maestro) {
                if($maestro->email != null){
                    $this->correo($maestro, $request->periodo_id);
                }
            }
            return response()->json('Exito');
        }else
            return response()->json('Error 1');
    }

    public function reenviar($id)
    {
        $encuesta = TokenHorario::find($id);
        $personal = $encuesta->personal;
        if($personal->email != null){
            $this->correo($personal, $encuesta->periodo_id);
        }

    }
}

<?php

namespace App\Http\Controllers;

use App\Academia;
use App\Periodo;
use App\Personal;
use Illuminate\Http\Request;

class PersonalControlador extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maestros = Personal::all();
        // $academias = Academia::all()->pluck('nombre', 'id');
        // $periodos = Periodo::all()->pluck('nombre', 'id');
        return view('maestros.consulta', compact('maestros'));
    }

    public function compartir()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $maestros = null;
        $academias = Academia::all()->pluck('nombre', 'id');
        return view('maestros.registro', compact('maestros', 'academias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
            if(Personal::create($request->all())){
                return response()->json('Exito');
            }else{
                return response()->json('Error');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $maestro = Personal::find($id);
        $academias = Academia::all()->pluck('nombre', 'id');
        return view('maestros.edicion', compact('maestro', 'academias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $personal = Personal::find($id);
        $personal->update($request->all());
        $personal->save();
        return response()->json('Exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $personal = Personal::find($id);
        if($personal->delete())
            return response()->json('Exito');
        else
            return response()->json('Error');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Periodo extends Model
{
	use SoftDeletes;
    protected $table = 'periodos';
	protected $fillable = ['nombre', 'inicio_periodo', 'final_periodo'];
	protected $dates = ['deleted_at'];
	public $timestamps = true;

	public function especialidades()
	{
		return $this->hasMany('App\Especialidad', 'periodo_id', 'id');
	}

	public function horarios()
	{
		return $this->hasMany('App\HorarioPersonal', 'periodo_id', 'id');
	}

	public function materias()
	{
		return $this->hasMany('App\MateriaPersonal', 'periodo_id', 'id');
	}

	public function token()
	{
		return $this->hasMany('App\TokenHorario', 'personal_id', 'id');
	}
}

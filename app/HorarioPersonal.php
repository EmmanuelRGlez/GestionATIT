<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorarioPersonal extends Model
{
	protected $table = 'horario_personals';
	protected $fillable = ['personal_id', 'periodo_id', 'dia', 'horario'];
	public $timestamps = false;

	public function profesor()
	{
		return $this->belongsTo('App\Personal');
	}

	public function periodo()
	{
		return $this->belongsTo('App\Periodo');
	}

}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MateriaPersonal extends Model
{
	protected $table = 'materia_personals';
	protected $fillable = ['personal_id', 'periodo_id', 'materia_id', 'optativa_id'];
	public $timestamps = false;

	public function profesor()
	{
		return $this->belongsTo('App\Personal');
	}

	public function periodo()
	{
		return $this->belongsTo('App\Periodo');
	}

	public function materia()
	{
		return $this->belongsTo('App\Materia');
	}

	public function optativa()
	{
		return $this->belongsTo('App\MateriaOptativa');
	}

}
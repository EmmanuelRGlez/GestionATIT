<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MateriaOptativa extends Model
{
    protected $table = 'materia_optativas';
	protected $fillable = ['nombre'];
	public $timestamps = false;

	public function materia_padre()
	{
		return $this->belongsTo('App\Materia');
	}
}

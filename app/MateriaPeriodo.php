<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MateriaPeriodo extends Model
{
    protected $table = 'materia_periodos';
	protected $fillable = ['periodo_id', 'materia_id', 'optativa_id'];
	public $timestamps = false;

	public function periodo()
	{
		return $this->belongsTo('App\Periodo');
	}

	public function materia()
	{
		return $this->belongsTo('App\Materia');
	}

	public function optativa()
	{
		return $this->belongsTo('App\MateriaOptativa');
	}
}

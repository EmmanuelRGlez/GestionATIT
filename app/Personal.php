<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Personal extends Model
{
	use SoftDeletes;
    protected $table = 'personals';
	protected $fillable = ['nombre', 'apellido_paterno', 'apellido_materno', 'telefono', 'email', 'academia_id'];
	protected $dates = ['deleted_at'];
	public $timestamps = true;

	public function academia()
	{
		return $this->belongsTo('App\Academia');
	}

	public function especialidades()
	{
		return $this->hasMany('App\Especialidad', 'personal_id', 'id');
	}

	public function token()
	{
		return $this->hasOne('App\TokenHorario', 'personal_id', 'id');
	}
}

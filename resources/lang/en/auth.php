<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'El correo y/o contraseña proporcionados no coinciden en nuestros registros.',
    'throttle' => 'Muchos intentos. Por favor, intenta nuevamente en :seconds segundos.',

];

@extends('layout.layout')

@section('title', 'Estadísticas')

@section('link')
	<link rel="stylesheet" type="text/css" href="/plugins/DataTables/datatables.css"/>
@endsection

@section('body')
	<div class="row">
		<div class="col s12">
			<div class="center">
				<h5><b>ESTADÍSTICAS</b></h5>
			</div>
			<div class="row">
				<div class="input-field col m12 s12">
					{!!Form::select('periodo_id', $periodos, null, ['id' => 'periodo_id'])!!}
					{!!Form::label('periodo_id', 'Periodo')!!}
				</div>
			</div>
	        <ul class="collapsible popout">
				<li class="active">
					<div class="collapsible-header">
						<i class="material-icons tooltipped left" data-position="right" data-tooltip="Edite los datos personales en caso de que estén erroneos.">info_outline</i>
						<b>Consulta de respuestas</b>
					</div>
					<div class="collapsible-body">
						<div class="row">
							<div div="tablaShoppers">
								<table id="example" class="striped" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Maestro</th>
											<th>Fecha de envío</th>
											<th>Fecha de respuesta</th>
											<th>Completada</th>
											<th width="11%">Acciones</th>
										</tr>
									</thead>
									<tbody>
										@if(!empty($encuestas))
											@if($encuestas->count() > 0)
												@foreach($encuestas as $encuesta)
													<tr id="{{$encuesta->id}}">
														<td>{{$encuesta->personal->nombre}} {{$encuesta->personal->apellido_paterno}} {{$encuesta->personal->apellido_materno}}</td>
														<td>{{$encuesta->created_at}}</td>
														<td>{{$encuesta->deleted_at}}</td>
														@if($encuesta->deleted_at == null)
															<td class="red-text"><i class="material-icons">remove</i></td>
															<td></td>
														@else
															<td class="green-text"><i class="material-icons">check</i></td>
															<td>
																<a href="/respuesta/{{$encuesta->id}}/detalles" class="tooltipped" data-position="top" data-tooltip="Ver detalle"><i class="material-icons blue-text">info</i></a>
															</td>

														@endif
													</tr>	
												@endforeach
											@endif
										@endif
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</li>
				<li class="active">
					<div class="collapsible-header">
						<i class="material-icons tooltipped left" data-position="right" data-tooltip="Edite los datos personales en caso de que estén erroneos.">info_outline</i>
						<b>Gráficas</b>
					</div>
					<div class="collapsible-body">
						<span id="span-graficas"></span>
						<canvas id="canvas" width="400px" height="100px"></canvas>
					</div>
				</li>
			</ul>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript" src="/plugins/DataTables/datatables.js"></script>
	<script type="text/javascript" src="/plugins/ChartJS/Chart.js"></script>
	<script type="text/javascript">
		$('.tooltipped').tooltip();
		$('.collapsible').collapsible({
    		accordion: false
    	});

		initTable(4);

		$('#periodo_id').change();

		function initTable(columna){
			$('#example').DataTable({
				"destroy": true,
				"dom": '<"left col s4"f><"top"rt><"bottom row"lip>',
				"lengthMenu": [[15, 25, 50, 100], [15, 25, 50, 100]],
				"aaSorting": [],
				"responsive": true,
				"columnDefs": [{
					"targets": columna,
					"orderable": false,
					"searchable": false,
				}]
			});
			$('select').formSelect();
		}

		$('#periodo_id').change(function(){
			var periodo_id = $('#periodo_id').val();
			$.get('/estadisticas/'+periodo_id+'/respuestas', function(response){
				if(response !== "null"){
					$.each(response, function(i, val){
						if(response[i].status == "Sí"){
							response[i].edit = '<a href="/respuesta/'+response[i].id+'/detalles" class="tooltipped" data-position="top" data-tooltip="Ver detalle"><i class="material-icons blue-text">info</i></a>';
							response[i].status = '<i class="material-icons green-text">check</i>';
						}else{
							response[i].status = '<i class="material-icons red-text">remove</i>';
							response[i].edit = '<button type="button" class="tooltipped" data-position="top" onclick="reenviarCorreo('+response[i].id+');" data-tooltip="Enviar recordatorio" class="btn btn-flat"><i class="material-icons blue-text">send</i></button>';
						}
				    });
					llenar(response);
				}else{
					
				}
			});

			$.get('/estadisticas/'+periodo_id+'/graficas', function(response){
				if(response == "null")
					$("#span-graficas").html("No hay datos para graficar.");
				else
					initChart(response.type, response.labels, response.data, response.backgroundColor);
			});
		});

		function reenviarCorreo(id){
			var route = '/horario/'+id+'/reenviar';
			var token = $('meta[name="csrf-token"]').attr('content');
			$.ajax({
				url: route,
				headers: {'X-CSRF-TOKEN': token},
				type: 'POST',
				dataType: 'json',
			}).done(function(response){
				if(response == "Exito"){
					var toastHTML = '<span>El correo ha sido reenviado.</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'blue darken-2',
					});
				}else if(response == "Error"){
					var toastHTML = '<span>El correo no fue reenviado.</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'red darken-2',
					});
				}
			}).fail(function(response){
				var toastHTML = '<span>El correo no fue reenviado.</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
				M.toast({
					html: toastHTML,
					displayLength: 5000,
					classes: 'red darken-2',
				});
			});
		}

		function llenar(response)
		{
			$('#example').DataTable({
				"destroy": true,
				"data": response,
				"dom": '<"left col s4"f><"top"rt><"bottom row"lip>',
				"lengthMenu": [[15, 25, 50, 100], [15, 25, 50, 100]],
				"aaSorting": [],
				"responsive": true,
				"columnDefs": [{
					"targets": 4,
					"orderable": false,
					"searchable": false,
				}],
				"columns":[
					{"data":"nombre"},
					{"data":"envio"},
					{"data":"respuesta"},
					{"data":"status"},
					{"data":"edit"},
				]
			});
			$('select').formSelect();
			$('.tooltipped').tooltip();
			// initTable(4);
		}


		function initChart(type, labels, data, backgroundColor){
			var ctx = document.getElementById('canvas').getContext('2d');
			var myChart = new Chart(ctx, {
				type: type,
				data: {
					labels: labels,
					datasets: [{
						label: 'Escala',
						data: data,
						backgroundColor: backgroundColor,
					}]
				},
				options: {
					scales: {
					},
					responsive: true,
					legend: {
						position: 'top',
					},
				}
			});
		}
	</script>
@endsection
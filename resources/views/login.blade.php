<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
	<title>ATIT - Inicio de sesión</title>

	<!-- CSS  -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	{{-- <link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/> --}}
	{{-- Etiqueta para herencia al head-link de la página --}}
</head>
<body>
	<div class="section">
		<div class="container">
			<h3 class="header center orange-text">Gestión de ATIT</h3>
			<div class="row center">
				<h6 class="header col s12 light">Proyecto de Sistemas Virtuales</h6>
			</div>
		</div>
	</div>

	<div class="container">
		{!!Form::open(['url' => '/', 'method' => 'post'])!!}
			{!!Form::hidden('_token', csrf_token(), ['id' => 'token'])!!}
			<div class="section">
				<div class="row">
					<div class="col s6 offset-s3">
						<div class="row red-text" align="center">
							@if($errors->any())
								<b>{{$errors->first()}}</b>
							@endif
						</div>
						<div class="card">
							<div class="card-content">
								<div class="row">
									<div class="input-field">
										{!!Form::email('email', null, ['id' => 'email', 'required'])!!}
										{!!Form::label('email', 'Correo')!!}
									</div>
								</div>
								<div class="row">
									<div class="input-field">
										{!!Form::password('password', ['id' => 'password', 'required', /*'minlength' => 8*/])!!}
										{!!Form::label('password', 'Contraseña')!!}
									</div>
								</div>
								<div class="row">
									<p>
										<label>
											{!!Form::checkbox('remember', null, false, ['id' => 'remember'])!!}
											<span>&nbsp;</span>
											<span>Recuerdame</span>
										</label>
									</p>
								</div>
								<div class="row" align="right">
									<button id="registro" class="waves-effect waves-light btn orange darken-2" type="submit"><b>INICIAR SESIÓN</b></button>
								</div>

							</div>
						</div>
					</div>
					
				</div>
			</div>
	</div>

	<div class="row">
		<br><br>
		<br><br>
		<br><br>
	</div>
	<footer class="page-footer blue darken-2">
		<div class="container">
			<div class="row">
				<div class="col l4 s12">
					<span class="col s12">Ricardo Emmanuel Guadalupe González González</span>
				</div>
				<div class="col l2 s12">
					<span class="col s12">José Pablo Pérez Estrada</span>
				</div>
				<div class="col l3 s12">
					<span class="col s12">Yair Alberto Castillo Jara</span>
				</div>
				<div class="col l3 s12">
					<span class="col s12">Francisco Hernández Martínez</span>
				</div>
			</div>
		</div>
		<div class="footer-copyright">
			<div class="container">
			Primavera 2018 <a class="blue-text text-lighten-3" href="http://upslp.edu.mx">UPSLP</a> 
			</div>
		</div>
	
	</footer>

	<!--  Scripts-->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="/js/materialize.js"></script>
	
	<script type="text/javascript" charset="utf-8" async defer>
	</script>
	</body>
</html>

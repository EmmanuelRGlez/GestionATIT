@extends('layout.layout_externo')

@section('title', 'Página no encontrada')

@section('body')
	<div class="content">
		<dic class="section">
			<div class="row" align="center">
				<div class="col s10 offset-s1 m8 offset-m2">
					<div class="card" style=" border-top: 4px solid #e77817;">
						<div class="card-content">
							<img src="/upslp_logo.png" width="40%">
						</div>
						<div class="card-content" align="center">
							<h1><b>404</b></h1>
							<h3>
								Lo que buscas no está disponible.
							</h3>
						</div>
						<div class="card-content">
							<div class="row" align="center">
								<h5 class="grey-text">
									Para cualquier duda o aclaración, contacte con el administrador.
								</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	{{-- <h2 class="header center">No se encuentra lo que está buscando.</h2>
	<br>
	<h5 class="header center">Caulquier inconveniente contacte a su administrador.</h5> --}}
@endsection
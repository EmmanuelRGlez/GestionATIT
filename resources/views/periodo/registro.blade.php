@extends('layout.layout')

@section('title', 'Registro de periodo')

@section('body')
	<div class="row">
		<div class="col s6 offset-s3">
			<div class="card darken-1">
				<div class="card-content center">
					<h5><b>REGISTRO DE PERIODO</b></h5>
				</div>
				<div class="card-content">
					<span class="card-title">Datos generales</span>
					@include('periodo.form.registro')
				</div>
				<div class="card-content" align="right">
				<button type="button" class="waves-effect waves-light btn-flat grey-text">CANCELAR</button>
				<button type="button" class="waves-effect waves-light btn orange darken-2" id="registro">GUARDAR</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript" src="/plugins/DataTables/datatables.js"></script>
	<script type="text/javascript">
		$('.tooltipped').tooltip();
		$('.datepicker').datepicker({
			format: 	'yyyy-mm-dd',
			autoClose: 	true,
			i18n: 		{
				cancel: 	'Cancelar',
				clear: 		'Borrar',
				done: 		'Aceptar',
				months:	[
					'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
					],
				monthsShort: [
					'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
					],
				weekdays: [
					'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'
					],
				weekdaysShort: [
				  	'Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'
				],
				weekdaysAbbrev:	[
					'D', 'L', 'M', 'X', 'J', 'V', 'S'
				]
			},
		});

		$('#registro').click(function(){
			registroPeriodo();
		});

		function registroPeriodo(){
			var route = '/periodos/registro';
			var token = $('meta[name="csrf-token"]').attr('content');

			var datos = {
				'nombre': 			$('#nombre').val(),
				'inicio_periodo': 	$('#inicio_periodo').val(),
				'final_periodo': 	$('#final_periodo').val(),
			};

			$.ajax({
				url: route,
				headers: {'X-CSRF-TOKEN': token},
				type: 'POST',
				dataType: 'json',
				data: datos,
			}).done(function(response){
				if(response == "Exito"){
					var toastHTML = '<span>El periodo ha sido registrado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'blue darken-2',
					});
				}else if(response == "Error"){
					var toastHTML = '<span>El periodo no ha sido registrado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'red darken-2',
					});
				}
			}).fail(function(response){
				var toastHTML = '<span>El periodo no ha sido registrado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
				M.toast({
					html: toastHTML,
					displayLength: 5000,
					classes: 'red darken-2',
				});
			});

		}
	</script>
@endsection
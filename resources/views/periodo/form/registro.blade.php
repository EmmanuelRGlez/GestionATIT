<div class="row">
	<div class="input-field col m12 s12">
		<i class="material-icons prefix">title</i>
		{!!Form::text('nombre', null, ['id' => 'nombre'])!!}
		{!!Form::label('nombre', 'Nombre')!!}
	</div>
</div>
<div class="row">
	<div class="input-field col m12 s12">
		<i class="material-icons prefix">event</i>
		{!!Form::text('inicio_periodo', null, ['id' => 'inicio_periodo', 'class' => 'datepicker'])!!}
		{!!Form::label('inicio_periodo', 'Inicio del periodo')!!}
	</div>
</div>
<div class="row">
	<div class="input-field col m12 s12">
		<i class="material-icons prefix">event</i>
		{!!Form::text('final_periodo', null, ['id' => 'final_periodo', 'class' => 'datepicker'])!!}
		{!!Form::label('final_periodo', 'Final del periodo')!!}
	</div>
</div>
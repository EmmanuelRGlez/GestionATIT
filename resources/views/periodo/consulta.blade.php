@extends('layout.layout')

@section('title', 'Consulta de periodos')

@section('link')
	<link rel="stylesheet" type="text/css" href="/plugins/DataTables/datatables.css"/>
@endsection

@section('body')
	<div class="row">
		<div class="col s8 offset-s1">
			<h5><b>CONSULTA DE PERIODOS</b></h5>
		</div>
		<div class="col s2" align="right" style="margin-top: 1rem">
			<a href="{{route('periodo.create')}}" class="waves-effect waves-light btn blue modal-trigger tooltipped" data-position="bottom" data-tooltip="Registrar nuevo periodo">
				<b>Registrar nuevo</b>
			</a>
		</div>
	</div>

	<div class="row">
	</div>
        
	<div class="row">
		<div div="tablaPeriodos" class="col s10 offset-s1">
			<table id="tablaPeriodo" class="striped" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Fecha de inicio</th>
						<th>Fecha de clausura</th>
						<th width="11%">Acciones</th>
					</tr>
				</thead>
				<tbody>
					@if(!empty($periodos))
						@if($periodos->count() > 0)
							@foreach($periodos as $periodo)
								<tr id="{{$periodo->id}}">
									<td>{{$periodo->nombre}}</td>
									<td>{{$periodo->inicio_periodo}}</td>
									<td>{{$periodo->final_periodo}}</td>
									<td>
										{{-- <a href="/periodos/{{$periodo->id}}/detalles" class="tooltipped" data-position="top" data-tooltip="Ver detalle"><i class="material-icons blue-text">check</i></a> --}}
										<a href="/periodos/{{$periodo->id}}/editar" class="tooltipped" data-position="top" data-tooltip="Editar"><i class="material-icons blue-text">mode_edit</i></a>
										<a href="#" onclick="borrarPeriodo({{$periodo->id}});" class="tooltipped" data-position="top" data-tooltip="Borrar"><i class="material-icons blue-text">delete</i></a>
									</td>
								</tr>	
							@endforeach
						@endif
					@endif
				</tbody>
			</table>
		</div>
	</div>

	<!-- Modal Structure -->
	<div id="modalConfirmacion" class="modal">
		<div class="modal-content">
			<h5>Confirmar eliminación</h5>
			<div class="row">
				<p>
					
				</p>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="modal-close waves-effect waves-orange btn-flat">Cancelar</button>
			<button type="button" class="modal-close waves-effect orange btn" id="confirmaBorrado">Borrar</button>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript" src="/plugins/DataTables/datatables.js"></script>
	<script type="text/javascript">
		var con = false;
		var global_id = null;
		initTable(3);
		$('.modal').modal();
		$('.tooltipped').tooltip();

		function borrarPeriodo(id){
			var instance = M.Modal.getInstance($('#modalConfirmacion').modal());
			instance.open();
			con = true;
			global_id = id;
		}
		
		function cancelarBorrado(){
			con = false;
			global_id = null;
		}

		function initTable(columna){
			$('#tablaPeriodo').DataTable({
				"dom": '<"left col s4"f><"top"rt><"bottom row"lip>',
				// "destroy": true,
				"lengthMenu": [[15, 25, 50, 100], [15, 25, 50, 100]],
				"aaSorting": [],
				"responsive": true,
				"columnDefs": [{
					"targets": columna,
					"orderable": false,
					"searchable": false,
				}]
			});
			$('select').formSelect();
		}
		$('#confirmaBorrado').click(function(){
			if(con && global_id != null){
				var route = '/periodos/'+global_id+'/eliminar';
				var token = $('meta[name="csrf-token"]').attr('content');

				$.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN': token},
					type: 'DELETE',
					dataType: 'json',
				}).done(function(response){
					if(response == "Exito"){
						var toastHTML = '<span>El periodo ha sido eliminado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
						M.toast({
							html: toastHTML,
							displayLength: 5000,
							classes: 'blue darken-2',
						});
						removerRegistro(global_id);
					}else if(response == "Error"){
						var toastHTML = '<span>El periodo no ha sido eliminado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
						M.toast({
							html: toastHTML,
							displayLength: 5000,
							classes: 'red darken-2',
						});
					}
				}).fail(function(response){
					var toastHTML = '<span>El periodo no ha sido eliminado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'red darken-2',
					});
				});
			}
		});

		function removerRegistro(id){
			$("#tablaPeriodo").DataTable().destroy();
			$("#"+id).remove();
			initTable(3);
		}
	</script>
@endsection
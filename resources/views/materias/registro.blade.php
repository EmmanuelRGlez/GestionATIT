@extends('layout.layout')

@section('title', 'Registro de materias')

@section('body')
	<div class="row">
		<div class="col s10 offset-s1">
			<div class="row center">
				<span class="card-title">
					<h5 class="header center"><b>REGISTRO DE MATERIAS POR PERDIODO</b></h5>
				</span>
				<div class="center">
					<h6 class="header col s12 light">A continuación seleccionará las materías que sean elegidas para que sean impartidas el periodo que se elija.</h6>
				</div>
			</div>
			<div class="row">
				<div class="input-field col m12 s12">
					<i class="material-icons prefix left">date_range</i>
					{!!Form::select('periodo_id', $periodos, null, ['id' => 'periodo_id'])!!}
					{!!Form::label('periodo_id', 'Periodo')!!}
				</div>
			</div>
			<div class="row">
				<ul class="collapsible popout">
					<li class="active">
						<div class="collapsible-header">
							<i class="material-icons left">language</i>
							<b>Inglés</b>
						</div>
						<div class="collapsible-body">
							@php
								$ingles = $materias->where('academia_id', 1);
							@endphp
							@foreach($ingles as $materia)
								<p><label class="black-text">
									{!!Form::checkbox('materias[]', $materia->id, false, ['id' => 'materia-'.$materia->id])!!}
									<span>&nbsp;</span>
									<span>{{$materia->nombre}}</span>
								</label></p>
							@endforeach
						</div>
				    </li>
				    <li class="active">
						<div class="collapsible-header">
							<i class="material-icons left">plus_one</i>
							<b>Matemáticas</b>
						</div>
						<div class="collapsible-body">
							@php
								$matematicas = $materias->where('academia_id', 2);
							@endphp
							@foreach($matematicas as $materia)
								<p><label class="black-text">
									{!!Form::checkbox('materias[]', $materia->id, false, ['id' => 'materia-'.$materia->id])!!}
									<span>&nbsp;</span>
									<span>{{$materia->nombre}}</span>
								</label></p>	
							@endforeach
						</div>
				    </li>
				    <li class="active">
						<div class="collapsible-header">
							<i class="material-icons left">format_color_fill</i>
							<b>Ciencias</b>
						</div>
						<div class="collapsible-body">
							@php
								$ciencias = $materias->where('academia_id', 3);
							@endphp
							@foreach($ciencias as $materia)
								<p><label class="black-text">
									{!!Form::checkbox('materias[]', $materia->id, false, ['id' => 'materia-'.$materia->id])!!}
									<span>&nbsp;</span>
									<span>{{$materia->nombre}}</span>
								</label></p>	
							@endforeach
						</div>
				    </li>
					<li class="active">
						<div class="collapsible-header">
							<i class="material-icons left">phonelink</i>
							<b>Área de tecnologías de la información</b>
						</div>
						<div class="collapsible-body">
							@php
								$iti = $materias->where('academia_id', 4)->where('carrera_id', 1);
								$item = $materias->where('academia_id', 4)->where('carrera_id', 2);
							@endphp
							<div class="row">
								<div class="col s12 m12 l6">
									<h6 class="header center"><b>Tecnologías de la información</b></h6>
									@foreach($iti as $materia)
										@if($materia->optativas->count() > 0)
											<p><label class="black-text">
												<span><b>{{$materia->nombre}}</b></span>
											</label></p>	
											@foreach($materia->optativas as $optativa)
												<p style="padding-left: 2rem"><label class="black-text">
													{!!Form::checkbox('optativas[]', $optativa->id, false, ['id' => $materia->id])!!}
													<span>&nbsp;</span>
													<span>{{$optativa->nombre}}</span>
												</label></p>
											@endforeach
										@else
											<p><label class="black-text">
												{!!Form::checkbox('materias[]', $materia->id, false, ['id' => 'materia-'.$materia->id])!!}
												<span>&nbsp;</span>
												<span>{{$materia->nombre}}</span>
											</label></p>	
										@endif
									@endforeach	
								</div>
								<div class="col s12 m12 l6">
									<h6 class="header center"><b>Telemática</b></h6>
									@foreach($item as $materia)
										@if($materia->optativas->count() > 0)
											<p><label class="black-text">
												<span><b>{{$materia->nombre}}</b></span>
											</label></p>	
											@foreach($materia->optativas as $optativa)
												<p style="padding-left: 2rem"><label class="black-text">
													{!!Form::checkbox('optativas[]', $optativa->id, false, ['id' => $materia->id])!!}
													<span>&nbsp;</span>
													<span>{{$optativa->nombre}}</span>
												</label></p>
											@endforeach
										@else
											<p><label class="black-text">
												{!!Form::checkbox('materias[]', $materia->id, false, ['id' => 'materia-'.$materia->id])!!}
												<span>&nbsp;</span>
												<span>{{$materia->nombre}}</span>
											</label></p>	
										@endif
									@endforeach
								</div>
							</div>
						</div>
				    </li>
				    <li class="active">
						<div class="collapsible-header">
							<i class="material-icons left">build</i>
							<b>Área de manufactura</b>
						</div>
						<div class="collapsible-body">
						</div>
				    </li>
				    <li class="active">
						<div class="collapsible-header">
							<i class="material-icons left">business</i>
							<b>Área de negocios</b>
						</div>
						<div class="collapsible-body">
						</div>
				    </li>
				</ul>
			</div>
			<div class="row">
				<div class="" align="right">
					<button type="button" class="waves-effect waves-light btn-flat grey-text">CANCELAR</button>
					<button type="button" class="waves-effect waves-light btn orange" id="registro">GUARDAR</button>
				</div> 
			</div>
		</div>
	</div>
@endsection

@section('script')
	{{-- <script type="text/javascript" src="/plugins/DataTables/datatables.js"></script> --}}
	<script type="text/javascript">
		$('.tooltipped').tooltip();
		$('.collapsible').collapsible({
			accordion: false
		});

		$('#registro').click(function(){
			registroMaterias();
		});

		function registroMaterias(){
			var route = '/materias/registro';
			var token = $('meta[name="csrf-token"]').attr('content');
			var materias = [];
			$("input[name='materias[]']:checked").each(function(i){
				materias[i] = this.value;
			});

			var optativas = [];
			$("input[name='optativas[]']:checked").each(function(i){
				optativas[i] = {
					materia_id: this.id,
					optativa_id: this.value
				};
			});
			var datos = {
				'materia_id': materias,
				'optativas': optativas,
				'periodo_id': $('#periodo_id').val(),
			};

			$.ajax({
				url: route,
				headers: {'X-CSRF-TOKEN': token},
				type: 'POST',
				dataType: 'json',
				data: datos,
			}).done(function(response){
				if(response == "Exito"){
					var toastHTML = '<span>El periodo con materias ha sido registrado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'blue darken-2',
					});
				}else if(response == "Error"){
					var toastHTML = '<span>El periodo con materias no ha sido registrado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'red darken-2',
					});
				}
			}).fail(function(response){
				var toastHTML = '<span>El periodo con materias no ha sido registrado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
				M.toast({
					html: toastHTML,
					displayLength: 5000,
					classes: 'red darken-2',
				});
			});
		}
	</script> 
@endsection
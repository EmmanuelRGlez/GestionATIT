@extends('layout.layout')

@section('title', 'Compartir horarios')

@section('body')
	<div class="row">
		<div class="col s8 offset-s2">
			<h5><b>COMPARTIR HORARIOS</b></h5>
		</div>
	</div>
	<div class="row">
		<div class="col s8 offset-s2">
			<div class="row">
				<div class="col s12">
					<ul class="tabs">
						<li class="tab col s6" class="active"><a href="#invitacionTodos">Enviar a todos</a></li>
						<li class="tab col s6"><a href="#invitacionAcademia">Enviar por academia</a></li>
					</ul>
				</div>
				<div id="invitacionTodos" class="col s12">
					<div class="row">
						<p align="center">
							Se enviará una invitación a todos los maestros que cuenten con correo electrónico registrados en el sistema para responder la encuesta de disponibilidad de horarios dentro del periodo seleccionado.
						</p>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<i class="material-icons prefix">timelapse</i>
							{!!Form::select('periodo_id', $periodos, null, ['id' => 'periodo_id-todos'])!!}
							{!!Form::label('periodo_id', 'Periodo')!!}
						</div>
					</div>
					<div class="row right">
						<button type="button" class="waves-effect waves-light btn orange" id="envioTodos">ENVIAR <i class="material-icons right">send</i></button>
					</div>
				</div>
				<div id="invitacionAcademia" class="col s12">
					<div class="row">
						<p align="center">
							Se enviará una invitación a todos los maestros que cuenten con correo electrónico registrados en el sistema para responder la encuesta de disponibilidad de horarios dentro del periodo y academia seleccionados.
						</p>
					</div>
					<div class="row">
						<div class="input-field col m6 s6">
							<i class="material-icons prefix">school</i>
							{!!Form::select('academia_id', $academias, null, ['id' => 'academia_id'])!!}
							{!!Form::label('academia_id', 'Academia')!!}
						</div>
						<div class="input-field col s6">
							<i class="material-icons prefix">timelapse</i>
							{!!Form::select('periodo_id', $periodos, null, ['id' => 'periodo_id'])!!}
							{!!Form::label('periodo_id', 'Periodo')!!}
						</div>
					</div>
					<div class="row right">
						<button type="button" class="waves-effect waves-light btn orange" id="envioAcademia">ENVIAR <i class="material-icons right">send</i></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row center">
		  <div class="preloader-wrapper big" id="preloader">
			<div class="spinner-layer spinner-blue-only">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
	</div>	
@endsection

@section('script')
	<script type="text/javascript">
		$('.tabs').tabs();
		var envio = false;

		$('#envioTodos').click(function(){
			if(!envio){
				envio = true;
				disableElem();		
				var route = '/horario/envio/todos';
				var token = $('meta[name="csrf-token"]').attr('content');
				var datos = {
					'periodo_id': $('#periodo_id-todos').val(),
				};

				var toastHTML = '<span>Esto puede tardar varios minutos...</span><button class="btn-flat toast-action white-text" onclick="dismiss(this);"><i class="material-icons">close</i></button>';
				M.toast({
					html: toastHTML,
					displayLength: 50000,
					classes: 'orange darken-2',
				});
				var toastHTML = '<span>Espere, por favor...</span><button class="btn-flat toast-action white-text" onclick="dismiss(this);"><i class="material-icons">close</i></button>';
				M.toast({
					html: toastHTML,
					displayLength: 50000,
					classes: 'orange darken-2',
				});

				$.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN': token},
					type: 'POST',
					dataType: 'json',
					data: datos,
				}).done(function(response){
					if(response == "Exito"){
						envio = false;
						enableElem();
						var toastHTML = '<span>Listo. Correo enviado.</span><button class="btn-flat toast-action white-text" onclick="dismiss(this);"><i class="material-icons">close</i></button>';
						M.toast({
							html: toastHTML,
							displayLength: 5000,
							classes: 'blue darken-2',
						});
					}else if(response == "Error"){
						envio = false;
						enableElem();
						var toastHTML = '<span>Hubo un problema al enviar los correos. Reintente, por favor.</span><button class="btn-flat toast-action white-text" onclick="dismiss(this);"><i class="material-icons">close</i></button>';
						M.toast({
							html: toastHTML,
							displayLength: 5000,
							classes: 'red darken-2',
						});
					}
				}).fail(function(response){
					envio = false;
					enableElem();
					var toastHTML = '<span>Hubo un problema al enviar los correos. Reintente, por favor.</span><button class="btn-flat toast-action white-text" onclick="dismiss(this);"><i class="material-icons">close</i></button>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'red darken-2',
					});
				});
			}
		});

		$('#envioAcademia').click(function(){
			if(!envio){
				envio = true;
				disableElem();
				var route = '/horario/envio/academia';
				var token = $('meta[name="csrf-token"]').attr('content');
				var datos = {
					'periodo_id': $('#periodo_id').val(),
					'academia_id': $('#academia_id').val(),
				};

				var toastHTML = '<span>Esto puede tardar varios minutos...</span><button class="btn-flat toast-action white-text" onclick="dismiss(this);"><i class="material-icons">close</i></button>';
				M.toast({
					html: toastHTML,
					displayLength: 50000,
					classes: 'orange darken-2',
				});
				var toastHTML = '<span>Espere, por favor...</span><button class="btn-flat toast-action white-text" onclick="dismiss(this);"><i class="material-icons">close</i></button>';
				M.toast({
					html: toastHTML,
					displayLength: 50000,
					classes: 'orange darken-2',
				});

				$.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN': token},
					type: 'POST',
					dataType: 'json',
					data: datos,
				}).done(function(response){
					if(response == "Exito"){
						envio = false;
						enableElem();
						var toastHTML = '<span>Listo. Correos enviados.</span><button class="btn-flat toast-action white-text" onclick="dismiss(this);"><i class="material-icons">close</i></button>';
						M.toast({
							html: toastHTML,
							displayLength: 5000,
							classes: 'blue darken-2',
						});
					}else if(response == "Error"){
						envio = false;
						enableElem();
						var toastHTML = '<span>Hubo un problema al enviar los correos. Reintente, por favor.</span><button class="btn-flat toast-action white-text" onclick="dismiss(this);"><i class="material-icons">close</i></button>';
						M.toast({
							html: toastHTML,
							displayLength: 5000,
							classes: 'red darken-2',
						});
					}else if(response == "Error 1"){
						envio = false;
						enableElem();
						var toastHTML = '<span>No hay maestros activos en ésta academia. Reintente, por favor.</span><button class="btn-flat toast-action white-text" onclick="dismiss(this);"><i class="material-icons">close</i></button>';
						M.toast({
							html: toastHTML,
							displayLength: 5000,
							classes: 'red darken-2',
						});
					}
				}).fail(function(response){
					envio = false;
					enableElem();
					var toastHTML = '<span>Hubo un problema al enviar los correos. Reintente, por favor.</span><button class="btn-flat toast-action white-text" onclick="dismiss(this);"><i class="material-icons">close</i></button>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'red darken-2',
					});
				});
			}
		});

		function disableElem(){
			M.Toast.dismissAll();
			$('#preloader').addClass('active');
			$('#envioTodos').addClass('disabled');
			$('#envioAcademia').addClass('disabled');
		}
		
		function enableElem(){
			M.Toast.dismissAll();
			$('#preloader').removeClass('active');
			$('#envioTodos').removeClass('disabled');
			$('#envioAcademia').removeClass('disabled');
		}

		function dismiss(){
			var toastElement = document.querySelector('.toast');
			var toastInstance = M.Toast.getInstance(toastElement);
			toastInstance.dismiss();
		}

	</script>

@endsection
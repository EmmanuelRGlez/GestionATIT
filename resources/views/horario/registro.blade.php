@extends('layout.layout_externo')

@section('title', 'Registro de disponibilidad')

@section('body')
	<div class="row">
		<div class="col s10 offset-s1">
			<div class="card">
				<div class="card-content grey lighten-3">
					<div class="row center">
						<img src="/upslp_logo.png" width="20%">
					</div>
					<div class="row center">
						<span class="card-title">
							<h5 class="header center"><b>Registro de disponibilidad</b></h5>
						</span>
						<div class="center">
							<h6 class="header col s12 light" id="h6-msj">A continuación seleccionará los días y las horas en los que podrá asistir a dar clases en el periodo de <b>{{$periodo->nombre}}</b></h6>
						</div>
					</div>
				</div>
			</div>
			<div id="content">
				<ul class="collapsible popout">
					<li class="active">
						<div class="collapsible-header">
							<i class="material-icons tooltipped left" data-position="right" data-tooltip="Edite los datos personales en caso de que estén erroneos.">info_outline</i>
							<b>Datos personales</b>
						</div>
						<div class="collapsible-body">
							{!!Form::model($personal)!!}
								@include('maestros.form.registro')
							{!!Form::close()!!}
						</div>
				    </li>
				    <li class="active">
						<div class="collapsible-header">
							<i class="material-icons tooltipped left" data-position="right" data-tooltip="Seleccione las horas y los días en los que tendrá disponibilidad para impartir clases para el periodo actual.">info_outline</i>
							<b>Horario</b>
						</div>
						<div class="collapsible-body">
							@include('horario.form.form_horario')
						</div>
				    </li>
				    <li class="active">
							<div class="collapsible-header">
								<i class="material-icons left">language</i>
								<b>Inglés</b>
							</div>
							<div class="collapsible-body">
								@php
								$ingles = $materias->filter(function ($item) {
									return $item->materia->academia_id == 1;
								});
								@endphp
								@foreach($ingles as $materia)
									<p><label class="black-text">
										{!!Form::checkbox('materias[]', $materia->materia->id, false, ['id' => 'materia-'.$materia->materia->id])!!}
										<span>&nbsp;</span>
										<span>{{$materia->materia->nombre}}</span>
									</label></p>
								@endforeach
							</div>
					    </li>
					    <li class="active">
							<div class="collapsible-header">
								<i class="material-icons left">plus_one</i>
								<b>Matemáticas</b>
							</div>
							<div class="collapsible-body">
								@php
								$ingles = $materias->filter(function ($item) {
									return $item->materia->academia_id == 2;
								});
								@endphp
								@foreach($ingles as $materia)
									<p><label class="black-text">
										{!!Form::checkbox('materias[]', $materia->materia->id, false, ['id' => 'materia-'.$materia->materia->id])!!}
										<span>&nbsp;</span>
										<span>{{$materia->materia->nombre}}</span>
									</label></p>
								@endforeach
							</div>
					    </li>
					    <li class="active">
							<div class="collapsible-header">
								<i class="material-icons left">format_color_fill</i>
								<b>Ciencias</b>
							</div>
							<div class="collapsible-body">
								@php
								$ingles = $materias->filter(function ($item) {
									return $item->materia->academia_id == 3;
								});
								@endphp
								@foreach($ingles as $materia)
									<p><label class="black-text">
										{!!Form::checkbox('materias[]', $materia->materia->id, false, ['id' => 'materia-'.$materia->materia->id])!!}
										<span>&nbsp;</span>
										<span>{{$materia->materia->nombre}}</span>
									</label></p>
								@endforeach
							</div>
					    </li>
						<li class="active">
							<div class="collapsible-header">
								<i class="material-icons left">phonelink</i>
								<b>Área de tecnologías de la información</b>
							</div>
							<div class="collapsible-body">
								@php
									$iti = $materias->filter(function ($i) {
										if($i->materia->carrera_id == 1)
											return $i->materia->academia_id == 4;
									});	

									$item = $materias->filter(function ($i) {
										if($i->materia->carrera_id == 2)
											return $i->materia->academia_id == 4;
									});		
								@endphp
								<div class="row">
									<div class="col s12 m12 l6">
										<h6 class="header center"><b>Tecnologías de la información</b></h6>
										@foreach($iti as $materia)
											@if($materia->optativa_id != null)
												<p><label class="black-text">
													<span><b>{{$materia->materia->nombre}}</b></span>
												</label></p>	
												<p style="padding-left: 2rem"><label class="black-text">
													{!!Form::checkbox('optativas[]', $materia->optativa->id, false, ['id' => $materia->materia_id])!!}
													<span>&nbsp;</span>
													<span>{{$materia->optativa->nombre}}</span>
												</label></p>
											@else
												<p><label class="black-text">
													{!!Form::checkbox('materias[]', $materia->materia->id, false, ['id' => 'materia-'.$materia->materia->id])!!}
													<span>&nbsp;</span>
													<span>{{$materia->materia->nombre}}</span>
												</label></p>	
											@endif
										@endforeach	
									</div>
									<div class="col s12 m12 l6">
										<h6 class="header center"><b>Telemática</b></h6>
										@foreach($item as $materia)
											@if($materia->optativa_id != null)
												<p><label class="black-text">
													<span><b>{{$materia->materia->nombre}}</b></span>
												</label></p>	
												<p style="padding-left: 2rem"><label class="black-text">
													{!!Form::checkbox('optativas[]', $materia->optativa->id, false, ['id' => $materia->materia_id])!!}
													<span>&nbsp;</span>
													<span>{{$materia->optativa->nombre}}</span>
												</label></p>
											@else
												<p><label class="black-text">
													{!!Form::checkbox('materias[]', $materia->materia->id, false, ['id' => 'materia-'.$materia->materia->id])!!}
													<span>&nbsp;</span>
													<span>{{$materia->materia->nombre}}</span>
												</label></p>	
											@endif
										@endforeach
									</div>
								</div>
							</div>
					    </li>
					    <li class="active">
							<div class="collapsible-header">
								<i class="material-icons left">build</i>
								<b>Área de manufactura</b>
							</div>
							<div class="collapsible-body">
							</div>
					    </li>
					    <li class="active">
							<div class="collapsible-header">
								<i class="material-icons left">business</i>
								<b>Área de negocios</b>
							</div>
							<div class="collapsible-body">
							</div>
					    </li>
				    {{-- <li class="active">
						<div class="collapsible-header">
							<i class="material-icons tooltipped left" data-position="right" data-tooltip="Seleccione las materias que impartirá en el periodo actual.">info_outline</i>
							<b>Materias</b>
						</div>
						<div class="collapsible-body">
							<div class="row">
								<div class="col s12">
									<div class="row">
										<div class="col s12" align="right">
											<button type="button" class="waves-effect waves-light btn grey modal-trigger" data-target="modal1">
												Seleccionar academias
											</button>
										</div>
									</div>		
									<div id="div-materias">
										@foreach($materias as $materia)
											<p><label>
													{!!Form::checkbox('materias[]', '', false, ['id' => 'materia-'.$materia->id])!!}
													<span>&nbsp;</span>
													<span>{{$materia->nombre}}</span>
											</label></p>
										@endforeach
									</div>
								</div>
							</div>
						</div>
				    </li> --}}
				</ul>	
				<div class="input-field col s12">
					<div class="card-content" align="right">
						<button type="button" class="waves-effect waves-light btn blue darken-2" id="registro">
							GUARDAR
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function(){
	    	$('.tabs').tabs();
	    	$('.tooltipped').tooltip();
	    	$('.collapsible').collapsible({
	    		accordion: false
	    	});
	    	$('.modal').modal();
	    	$('#academia_id').append('<option disabled>SELECCIONE UNA OPCIÓN</option>');
	    	$('select').formSelect();
		});

		// $('#seleccionarAcademias').click(function(){
		// 	$('#div-materias').empty();
		// 	var academias = $('[name="academias[]"]').serializeArray();
		// 	$.each(academias, function(i, val){
		// 		$.get('/academias/'+val.value, function(response){
		// 			$.each(response, function(j, value){
		// 				var template = 
		// 				'<p><label>'+
		// 					'<input id="materia-'+value.id+'" name="materias[]" value="" type="checkbox">'+
		// 					'<span>&nbsp;</span>'+
		// 					'<span>'+value.nombre+'</span>'
		// 				'</label></p>';
		// 				$('#div-materias').append(template);
		// 			});
		// 		});
		// 	});
		// });


		$('#registro').click(function(){
			registro();
		});

		function registro(){
			var route = '/horario/{{$token}}/registro';
			var token = $('meta[name="csrf-token"]').attr('content');

			var lunes = []; martes = []; miercoles = []; jueves = []; viernes = []; sabado = [];

			var materias = [];
			$("input[name='materias[]']:checked").each(function(i){
				materias[i] = this.value;
			});

			var optativas = [];
			$("input[name='optativas[]']:checked").each(function(i){
				optativas[i] = {
					materia_id: this.id,
					optativa_id: this.value
				};
			});

			$("input[name='lunes[]']:checked").each(function(i){
				lunes[i] = this.value;
			});
			$("input[name='martes[]']:checked").each(function(i){
				martes[i] = this.value;
			});
			$("input[name='miercoles[]']:checked").each(function(i){
				miercoles[i] = this.value;
			});
			$("input[name='jueves[]']:checked").each(function(i){
				jueves[i] = this.value;
			});
			$("input[name='viernes[]']:checked").each(function(i){
				viernes[i] = this.value;
			});
			$("input[name='sabado[]']:checked").each(function(i){
				sabado[i] = this.value;
			});

			var datos = {
				nombre: $('#nombre').val(),
				apellido_paterno: $('#apellido_paterno').val(),
				apellido_materno: $('#apellido_materno').val(),
				telefono: $('#telefono').val(),
				email: $('#email').val(),
				academia_id: $('#academia_id').val(),
				licenciatura: $('#licenciatura').val(),
				maestria: $('#maestria').val(),
				experiencia_profesional: $('#experiencia_profesional').val(),
				experiencia_docente: $('#experiencia_docente').val(),
				lunes: lunes,
				martes: martes,
				miercoles: miercoles,
				jueves: jueves,
				viernes: viernes,
				sabado: sabado,
				materia_id: materias,
				optativas: optativas,
			};

			$.ajax({
				url: route,
				headers: {'X-CSRF-TOKEN': token},
				type: 'POST',
				dataType: 'json',
				data: datos,
			}).done(function(response){
				if(response == "Exito"){
					var toastHTML = '<span>El horario ha sido enviado. ¡Gracias!</span>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'blue darken-2',
					});
					$('#content').empty();
					$('#h6-msj').html('MUCHAS GRACIAS POR PARTICIPAR EN LA ENCUESTA.');
				}else if(response == "Error"){
					var toastHTML = '<span>El horario no ha sido enviado. ¡Intentelo más tarde!</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'red darken-2',
					});
				}
			}).fail(function(response){
				var toastHTML = '<span>El horario no ha sido enviado. ¡Intentelo más tarde!</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
				M.toast({
					html: toastHTML,
					displayLength: 5000,
					classes: 'red darken-2',
				});
			});
		}
	</script>
@endsection
@extends('layout.layout')

@section('title', 'Detalle de encuesta')

@section('body')
	<div id="content">
				<ul class="collapsible popout">
					<li class="active">
						<div class="collapsible-header">
							<b>Datos personales</b>
						</div>
						<div class="collapsible-body">
							{!!Form::model($personal)!!}
							<div class="row">
								<div class="col m3 s3">
									<b>Nombre:</b><br>
									{{$personal->nombre}}
								</div>
								<div class="col m3 s3">
									<b>Apellido Paterno:</b><br>
									{{$personal->apellido_paterno}}
								</div>
								<div class="col m3 s3">
									<b>Apellido Materno:</b><br>
									{{$personal->apellido_materno}}
								</div>
								<div class="col m3 s3">
									<b>Teléfono:</b><br>
									{{$personal->telefono}}
								</div>
								<div class="col m3 s3">
									<b>E-mail:</b><br>
									{{$personal->email}}
								</div>
								<div class="col m3 s3">
									<b>Academia:</b><br>
									{{$personal->academia->nombre}}
								</div>
								@if(!empty($especialidad))
									<div class="col m3 s3">
										<b>Licenciatura:</b><br>
										{{$especialidad->licenciatura}}
									</div>
									<div class="col m3 s3">
										<b>Maestría:</b><br>
										{{$especialidad->maestria}}
									</div>
									<div class="col m3 s3">
										<b>Experiencia Docente:</b><br>
										{{$especialidad->experiencia_docente}} año(s)
									</div>
									<div class="col m3 s3">
										<b>Experiencia profesional:</b><br>
										{{$especialidad->experiencia_profesional}} año(s)
									</div>
								@endif
							</div>
								{{-- @include('maestros.form.registro') --}}
							{!!Form::close()!!}
						</div>
				    </li>
				    <li class="active">
						<div class="collapsible-header">
							<b>Horario</b>
						</div>
						<div class="collapsible-body">
							@php
								$semana['Lunes'] = $horarios->filter(function ($item) {
									return $item->dia == "Lunes";
								});
								$semana['Martes'] = $horarios->filter(function ($item) {
									return $item->dia == "Martes";
								});
								$semana['Miércoles'] = $horarios->filter(function ($item) {
									return $item->dia == "Miércoles";
								});
								$semana['Jueves'] = $horarios->filter(function ($item) {
									return $item->dia == "Jueves";
								});
								$semana['Viernes'] = $horarios->filter(function ($item) {
									return $item->dia == "Viernes";
								});
								$semana['Sábado'] = $horarios->filter(function ($item) {
									return $item->dia == "Sábado";
								});
							@endphp
							<div class="row">
								@foreach($semana as $dia => $horarios)
								<div class="col m2 l2 s6">
									<b>{{$dia}}</b><br>
									@foreach($horarios as $horario)
										<span style="padding-left: 1.5rem;">{{$horario->horario}}</span>
									@endforeach
									<br>
								</div>
								@endforeach
							</div>

						</div>
				    </li>
				    <li class="active">
							<div class="collapsible-header">
								<i class="material-icons left">language</i>
								<b>Inglés</b>
							</div>
							<div class="collapsible-body">
								@php
								$ingles = $materias->filter(function ($item) {
									return $item->materia->academia_id == 1;
								});
								@endphp
								@foreach($ingles as $materia)
									<p>
										<span>{{$materia->materia->nombre}}</span>
									</p>
								@endforeach
							</div>
					    </li>
					    <li class="active">
							<div class="collapsible-header">
								<i class="material-icons left">plus_one</i>
								<b>Matemáticas</b>
							</div>
							<div class="collapsible-body">
								@php
									$matematicas = $materias->filter(function ($item) {
										return $item->materia->academia_id == 2;
									});
								@endphp
								@foreach($matematicas as $materia)
									<p>
										<span>{{$materia->materia->nombre}}</span>
									</p>
								@endforeach
							</div>
					    </li>
					    <li class="active">
							<div class="collapsible-header">
								<i class="material-icons left">format_color_fill</i>
								<b>Ciencias</b>
							</div>
							<div class="collapsible-body">
								@php
									$ciencias = $materias->filter(function ($item) {
										return $item->materia->academia_id == 3;
									});
								@endphp
								@foreach($ciencias as $materia)
									<p>
										<span>{{$materia->materia->nombre}}</span>
									</p>
								@endforeach
							</div>
					    </li>
						<li class="active">
							<div class="collapsible-header">
								<i class="material-icons left">phonelink</i>
								<b>Área de tecnologías de la información</b>
							</div>
							<div class="collapsible-body">
								@php
									$iti = $materias->filter(function ($i) {
										if($i->materia->carrera_id == 1)
											return $i->materia->academia_id == 4;
									});	

									$item = $materias->filter(function ($i) {
										if($i->materia->carrera_id == 2)
											return $i->materia->academia_id == 4;
									});		
								@endphp
								<div class="row">
									<div class="col s12 m12 l6">
										<h6 class="header center"><b>Tecnologías de la información</b></h6>
										@foreach($iti as $materia)
											@if($materia->optativa_id != null)
												<p>
													<span><b>{{$materia->materia->nombre}}</b></span>
												</p>	
												<p style="padding-left: 2rem">
													<span>{{$materia->optativa->nombre}}</span>
												</p>
											@else
												<p>
													<span>{{$materia->materia->nombre}}</span>
												</p>	
											@endif
										@endforeach	
									</div>
									<div class="col s12 m12 l6">
										<h6 class="header center"><b>Telemática</b></h6>
										@foreach($item as $materia)
											@if($materia->optativa_id != null)
												<p>
													<span><b>{{$materia->materia->nombre}}</b></span>
												</p>	
												<p style="padding-left: 2rem">
													<span>{{$materia->optativa->nombre}}</span>
												</label></p>
											@else
												<p>
													<span>{{$materia->materia->nombre}}</span>
												</p>	
											@endif
										@endforeach
									</div>
								</div>
							</div>
					    </li>
					    <li class="active">
							<div class="collapsible-header">
								<i class="material-icons left">build</i>
								<b>Área de manufactura</b>
							</div>
							<div class="collapsible-body">
							</div>
					    </li>
					    <li class="active">
							<div class="collapsible-header">
								<i class="material-icons left">business</i>
								<b>Área de negocios</b>
							</div>
							<div class="collapsible-body">
							</div>
					    </li>
				</ul>	
				<div class="input-field col s12">
					<div class="card-content" align="right">
						<button type="button" class="waves-effect waves-light btn blue darken-2" id="registro">
							GUARDAR
						</button>
					</div>
				</div>
			</div>
@endsection

@section('script')
	<script type="text/javascript">
		$('.collapsible').collapsible({
    		accordion: false
    	});
	</script>
@endsection
<div class="row">
	<div class="col s12">
		<ul class="tabs">
			<li class="tab col s6 m6"><a class="active" href="#matutino">Horario Matutino</a></li>
			<li class="tab col s6 m6"><a href="#vespertino">Horario vespertino</a></li>
		</ul>
    </div>
	<div id="matutino" class="col s12">
		{{-- <h6 class="header center blue-text">Por las mañanas:</h6> --}}
		<div class="row" style="margin-top: 2%;">			
			<div class="col m12 offset-m2" style="font-size: 12px;">
				<div class="col m1"><span>Hora</span></div>
				<div class="col m1"><span>07:00 - 8:00</span></div>
				<div class="col m1"><span>08:00 - 09:00</span></div>
				<div class="col m1"><span>09:00 - 10:00</span></div>
				<div class="col m1"><span>10:00 - 11:00</span></div>
				<div class="col m1"><span>11:00 - 12:00</span></div>
				<div class="col m1"><span>12:00 - 13:00</span></div>
				<div class="col m1"><span>13:00 - 14:00</span></div>
			</div>
		</div>
		<div class="row">			
			<div class="col m12 offset-m2" style="font-size: 12px;">
				<div class="col m1"><span>Lunes</span></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "07:00 - 8:00", false, ['id' => 'lunes-07'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "08:00 - 09:00", false, ['id' => 'lunes-08'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "09:00 - 10:00", false, ['id' => 'lunes-09'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "10:00 - 11:00", false, ['id' => 'lunes-10'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "11:00 - 12:00", false, ['id' => 'lunes-11'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "12:00 - 13:00", false, ['id' => 'lunes-12'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "13:00 - 14:00", false, ['id' => 'lunes-13'])!!}<span></span></label></p></div>
			</div>
		</div>
		<div class="row">			
			<div class="col m12 offset-m2" style="font-size: 12px;">
				<div class="col m1"><span>Martes</span></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "07:00 - 8:00", false, ['id' => 'martes-07'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "08:00 - 09:00", false, ['id' => 'martes-08'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "09:00 - 10:00", false, ['id' => 'martes-09'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "10:00 - 11:00", false, ['id' => 'martes-10'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "11:00 - 12:00", false, ['id' => 'martes-11'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "12:00 - 13:00", false, ['id' => 'martes-12'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "13:00 - 14:00", false, ['id' => 'martes-13'])!!}<span></span></label></p></div>
			</div>
		</div>
		<div class="row">			
			<div class="col m12 offset-m2" style="font-size: 12px;">
				<div class="col m1"><span>Miércoles</span></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "07:00 - 8:00", false, ['id' => 'miercoles-07'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "08:00 - 09:00", false, ['id' => 'miercoles-08'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "09:00 - 10:00", false, ['id' => 'miercoles-09'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "10:00 - 11:00", false, ['id' => 'miercoles-10'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "11:00 - 12:00", false, ['id' => 'miercoles-11'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "12:00 - 13:00", false, ['id' => 'miercoles-12'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "13:00 - 14:00", false, ['id' => 'miercoles-13'])!!}<span></span></label></p></div>
			</div>
		</div>
		<div class="row">			
			<div class="col m12 offset-m2" style="font-size: 12px;">
				<div class="col m1"><span>Jueves</span></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "07:00 - 8:00", false, ['id' => 'jueves-07'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "08:00 - 09:00", false, ['id' => 'jueves-08'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "09:00 - 10:00", false, ['id' => 'jueves-09'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "10:00 - 11:00", false, ['id' => 'jueves-10'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "11:00 - 12:00", false, ['id' => 'jueves-11'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "12:00 - 13:00", false, ['id' => 'jueves-12'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "13:00 - 14:00", false, ['id' => 'jueves-13'])!!}<span></span></label></p></div>
			</div>
		</div>
		<div class="row">			
			<div class="col m12 offset-m2" style="font-size: 12px;">
				<div class="col m1"><span>Viernes</span></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "07:00 - 8:00", false, ['id' => 'viernes-07'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "08:00 - 09:00", false, ['id' => 'viernes-08'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "09:00 - 10:00", false, ['id' => 'viernes-09'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "10:00 - 11:00", false, ['id' => 'viernes-10'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "11:00 - 12:00", false, ['id' => 'viernes-11'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "12:00 - 13:00", false, ['id' => 'viernes-12'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "13:00 - 14:00", false, ['id' => 'viernes-13'])!!}<span></span></label></p></div>
			</div>
		</div>
		<div class="row">			
			<div class="col m12 offset-m2" style="font-size: 12px;">
				<div class="col m1"><span>Sábado</span></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('sabado[]', "07:00 - 8:00", false, ['id' => 'sabado-07'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('sabado[]', "08:00 - 09:00", false, ['id' => 'sabado-08'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('sabado[]', "09:00 - 10:00", false, ['id' => 'sabado-09'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('sabado[]', "10:00 - 11:00", false, ['id' => 'sabado-10'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('sabado[]', "11:00 - 12:00", false, ['id' => 'sabado-11'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('sabado[]', "12:00 - 13:00", false, ['id' => 'sabado-12'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('sabado[]', "13:00 - 14:00", false, ['id' => 'sabado-13'])!!}<span></span></label></p></div>
			</div>
		</div>
	</div>
	<div id="vespertino" class="col s12">
		{{-- <h6 class="header center blue-text">Por las tardes:</h6> --}}
		<div class="row" style="margin-top: 2%;">				
			<div class="col m12 offset-m2" style="font-size: 12px;">
				<div class="col m1">
					<span>Hora</span>
				</div>
				<div class="col m1"><span>14:00 - 15:00</span></div>
				<div class="col m1"><span>15:00 - 16:00</span></div>
				<div class="col m1"><span>16:00 - 17:00</span></div>
				<div class="col m1"><span>17:00 - 18:00</span></div>
				<div class="col m1"><span>18:00 - 19:00</span></div>
				<div class="col m1"><span>19:00 - 20:00</span></div>
				<div class="col m1"><span>20:00 - 21:00</span></div>
			</div>
		</div>
		<div class="row">			
			<div class="col m12 offset-m2" style="font-size: 12px;">
				<div class="col m1"><span>Lunes</span></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "14:00 - 15:00", false, ['id' => 'lunes-14'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "15:00 - 16:00", false, ['id' => 'lunes-15'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "16:00 - 17:00", false, ['id' => 'lunes-16'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "17:00 - 18:00", false, ['id' => 'lunes-17'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "18:00 - 19:00", false, ['id' => 'lunes-18'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "19:00 - 20:00", false, ['id' => 'lunes-19'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('lunes[]', "20:00 - 21:00", false, ['id' => 'lunes-20'])!!}<span></span></label></p></div>
			</div>
		</div>
		<div class="row">			
			<div class="col m12 offset-m2" style="font-size: 12px;">
				<div class="col m1"><span>Martes</span></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "14:00 - 15:00", false, ['id' => 'martes-14'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "15:00 - 16:00", false, ['id' => 'martes-15'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "16:00 - 17:00", false, ['id' => 'martes-16'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "17:00 - 18:00", false, ['id' => 'martes-17'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "18:00 - 19:00", false, ['id' => 'martes-18'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "19:00 - 20:00", false, ['id' => 'martes-19'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('martes[]', "20:00 - 21:00", false, ['id' => 'martes-20'])!!}<span></span></label></p></div>
			</div>
		</div>
		<div class="row">			
			<div class="col m12 offset-m2" style="font-size: 12px;">
				<div class="col m1"><span>Miércoles</span></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "14:00 - 15:00", false, ['id' => 'miercoles-14'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "15:00 - 16:00", false, ['id' => 'miercoles-15'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "16:00 - 17:00", false, ['id' => 'miercoles-16'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "17:00 - 18:00", false, ['id' => 'miercoles-17'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "18:00 - 19:00", false, ['id' => 'miercoles-18'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "19:00 - 20:00", false, ['id' => 'miercoles-19'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('miercoles[]', "20:00 - 21:00", false, ['id' => 'miercoles-20'])!!}<span></span></label></p></div>
			</div>
		</div>
		<div class="row">			
			<div class="col m12 offset-m2" style="font-size: 12px;">
				<div class="col m1"><span>Jueves</span></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "14:00 - 15:00", false, ['id' => 'jueves-14'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "15:00 - 16:00", false, ['id' => 'jueves-15'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "16:00 - 17:00", false, ['id' => 'jueves-16'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "17:00 - 18:00", false, ['id' => 'jueves-17'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "18:00 - 19:00", false, ['id' => 'jueves-18'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "19:00 - 20:00", false, ['id' => 'jueves-19'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('jueves[]', "20:00 - 21:00", false, ['id' => 'jueves-20'])!!}<span></span></label></p></div>
			</div>
		</div>
		<div class="row">			
			<div class="col m12 offset-m2" style="font-size: 12px;">
				<div class="col m1"><span>Viernes</span></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "14:00 - 15:00", false, ['id' => 'viernes-14'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "15:00 - 16:00", false, ['id' => 'viernes-15'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "16:00 - 17:00", false, ['id' => 'viernes-16'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "17:00 - 18:00", false, ['id' => 'viernes-17'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "18:00 - 19:00", false, ['id' => 'viernes-18'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "19:00 - 20:00", false, ['id' => 'viernes-19'])!!}<span></span></label></p></div>
				<div class="col m1"><p align="center"><label>{!!Form::checkbox('viernes[]', "20:00 - 21:00", false, ['id' => 'viernes-20'])!!}<span></span></label></p></div>
			</div>
		</div>
	</div>
</div>
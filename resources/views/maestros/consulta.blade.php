@extends('layout.layout')

@section('title', 'Consulta maestros')

@section('link')
	<link rel="stylesheet" type="text/css" href="/plugins/DataTables/datatables.css"/>
@endsection

@section('body')
	<div class="row">
		<div class="col s8 offset-s1">
			<h5><b>CONSULTA DE MAESTROS</b></h5>
		</div>
		<div class="col s2" align="right" style="margin-top: 1rem">
			<a href="{{route('personal.create')}}" class="waves-effect waves-light btn blue modal-trigger tooltipped" data-position="bottom" data-tooltip="Registrar nuevo maestro">
				<b>Registrar nuevo</b>
			</a>
		</div>
	</div>

	<div class="row">
	</div>
        
	<div class="row">
		<div div="tablaShoppers" class="col s10 offset-s1">
			<table id="example" class="striped" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Exp. Perofesional</th>
						<th>Exp. Docente</th>
						<th>Licenciatura</th>
						<th>Maestria</th>
						<th>Email</th>
						<th width="11%">Acciones</th>
					</tr>
				</thead>
				<tbody>
					@if(!empty($maestros))
						@if($maestros->count() > 0)
							@foreach($maestros as $maestro)
								<tr id="{{$maestro->id}}">
									<td>{{$maestro->nombre}} {{$maestro->apellido_paterno}} {{$maestro->apellido_materno}}</td>
									@if($maestro->especialidades->count() > 0)
										<td>{{$maestro->especialidades[0]->experiencia_profesional}}</td>
										<td>{{$maestro->especialidades[0]->experiencia_docente}}</td>
										<td>{{$maestro->especialidades[0]->licenciatura}}</td>
										<td>{{$maestro->especialidades[0]->maestria}}</td>
									@else
										<td>Sin registrar</td>
										<td>Sin registrar</td>
										<td>Sin registrar</td>
										<td>Sin registrar</td>
									@endif
									<td>{{$maestro->email}}</td>
									<td>
										{{-- <a href="/personal/{{$maestro->id}}/detalles" class="tooltipped" data-position="top" data-tooltip="Ver detalle"><i class="material-icons blue-text">info</i></a> --}}
										<a href="/personal/{{$maestro->id}}/editar" class="tooltipped" data-position="top" data-tooltip="Editar"><i class="material-icons blue-text">mode_edit</i></a>
										<a href="#" onclick="borrarMaestro({{$maestro->id}});" class="tooltipped" data-position="top" data-tooltip="Borrar"><i class="material-icons blue-text">delete</i></a>
									</td>
								</tr>	
							@endforeach
						@endif
					@endif
				</tbody>
			</table>
		</div>
	</div>

	<!-- Modal Structure -->
	<div id="modalConfirmacion" class="modal">
		<div class="modal-content">
			<h5>Confirmar eliminación</h5>
			<div class="row">
				<p>
					
				</p>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="modal-close waves-effect waves-orange btn-flat">Cancelar</button>
			<button type="button" class="modal-close waves-effect orange btn" id="confirmaBorrado">Borrar</button>
		</div>
	</div>
@endsection

@section('body-end')
{{-- 
	<div class="fixed-action-btn" style="bottom: 45px; right: 45px;">
		<a href="{{route('personal.create')}}" class="btn-floating btn-large blue tooltipped">
			<i class="large material-icons">add</i>
		</a>
		<ul>
			<li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a></li>
			<li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a></li>
			<li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
			<li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
		</ul> 
--}}
	</div>
@endsection

@section('script')
	<script type="text/javascript" src="/plugins/DataTables/datatables.js"></script>
	<script type="text/javascript">
		var con = false;
		var global_id = null;
		initTable(6);
		$('.modal').modal();
		$('.tooltipped').tooltip();

		function borrarMaestro(id){
			var instance = M.Modal.getInstance($('#modalConfirmacion').modal());
			instance.open();
			con = true;
			global_id = id;
		}
		
		function cancelarBorrado(){
			con = false;
			global_id = null;
		}

		$('#confirmaBorrado').click(function(){
			if(con && global_id != null){
				var route = '/personal/'+global_id+'/eliminar';
				var token = $('meta[name="csrf-token"]').attr('content');

				$.ajax({
					url: route,
					headers: {'X-CSRF-TOKEN': token},
					type: 'DELETE',
					dataType: 'json',
				}).done(function(response){
					if(response == "Exito"){
						var toastHTML = '<span>El maestro ha sido eliminado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
						M.toast({
							html: toastHTML,
							displayLength: 5000,
							classes: 'blue darken-2',
						});
						removerRegistro(global_id);
					}else if(response == "Error"){
						var toastHTML = '<span>El maestro no ha sido eliminado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
						M.toast({
							html: toastHTML,
							displayLength: 5000,
							classes: 'red darken-2',
						});
					}
				}).fail(function(response){
					var toastHTML = '<span>El maestro no ha sido eliminado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'red darken-2',
					});
				});
			}
		});

		function initTable(columna){
			$('#example').DataTable({
				"dom": '<"left col s4"f><"top"rt><"bottom row"lip>',
				"lengthMenu": [[15, 25, 50, 100], [15, 25, 50, 100]],
				"aaSorting": [],
				"responsive": true,
				"columnDefs": [{
					"targets": columna,
					"orderable": false,
					"searchable": false,
				}]
			});
			$('select').formSelect();
		}

		function removerRegistro(id){
			$("#example").DataTable().destroy();
			$("#"+id).remove();
			initTable(3);
		}
	</script>
@endsection
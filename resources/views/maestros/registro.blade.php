@extends('layout.layout')

@section('title', 'Registro de maestros')

@section('body')
	<div class="row">
		<div class="col s6 offset-s3">
			<div class="card darken-1">
				<div class="card-content center">
					<h5><b>REGISTRO DE MAESTRO</b></h5>
				</div>
				<div class="card-content">
					<span class="card-title">Datos Personales</span>
					@include('maestros.form.registro')
				</div>
				<div class="card-content" align="right">
					<button type="button" class="waves-effect waves-light btn-flat grey-text">CANCELAR</button>
					<button type="button" class="waves-effect waves-light btn orange" id="registro">GUARDAR</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script type="text/javascript" src="/plugins/DataTables/datatables.js"></script>
	<script type="text/javascript">
		$('.tooltipped').tooltip();

		$('#registro').click(function(){
			registroPersonal();
		});

		function registroPersonal(){
			var route = '/personal/registro';
			var token = $('meta[name="csrf-token"]').attr('content');

			var datos = {
				'nombre': 			$('#nombre').val(),
				'apellido_paterno': $('#apellido_paterno').val(),
				'apellido_materno': $('#apellido_materno').val(),
				'telefono': 		$('#telefono').val(),
				'email': 			$('#email').val(),
				'academia_id': 		$('#academia_id').val(),
			};

			$.ajax({
				url: route,
				headers: {'X-CSRF-TOKEN': token},
				type: 'POST',
				dataType: 'json',
				data: datos,
			}).done(function(response){
				if(response == "Exito"){
					var toastHTML = '<span>El maestro ha sido registrado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'blue darken-2',
					});
				}else if(response == "Error"){
					var toastHTML = '<span>El maestro no ha sido registrado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
					M.toast({
						html: toastHTML,
						displayLength: 5000,
						classes: 'red darken-2',
					});
				}
			}).fail(function(response){
				var toastHTML = '<span>El maestro no ha sido registrado</span><button class="btn-flat toast-action white-text"><i class="material-icons">close</i></button>';
				M.toast({
					html: toastHTML,
					displayLength: 5000,
					classes: 'red darken-2',
				});
			});

		}
	</script>
@endsection
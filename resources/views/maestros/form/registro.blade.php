<div class="row">
	<div class="input-field col m12 s12">
		<i class="material-icons prefix">account_circle</i>
		{!!Form::text('nombre', null, ['id' => 'nombre'])!!}
		{!!Form::label('nombre', 'Nombre')!!}
	</div>
</div>
<div class="row">
	<div class="input-field col m6 s6">
		{!!Form::text('apellido_paterno', null, ['id' => 'apellido_paterno'])!!}
		{!!Form::label('apellido_paterno', 'Apellido Paterno')!!}
	</div>
	<div class="input-field col m6 s6">
		{!!Form::text('apellido_materno', null, ['id' => 'apellido_materno'])!!}
		{!!Form::label('apellido_materno', 'Apellido Materno')!!}
	</div>
</div>
<div class="row">
	<div class="input-field col m6 s6">
		<i class="material-icons prefix">local_phone</i>
		{!!Form::text('telefono', null, ['id' => 'telefono'])!!}
		{!!Form::label('telefono', 'Teléfono')!!}
	</div>
	<div class="input-field col m6 s6">
		<i class="material-icons prefix">email</i>
		{!!Form::text('email', null, ['id' => 'email'])!!}
		{!!Form::label('email', 'E-mail')!!}
	</div>
</div>
@if(!empty($personal))
	@if(!empty($especialidad))
		{!!Form::model($especialidad)!!}
	@endif
		<div class="row">
			<div class="input-field col m6 s6">
				<i class="material-icons prefix">school</i>
				{!!Form::text('licenciatura', null, ['id' => 'licenciatura'])!!}
				{!!Form::label('licenciatura', 'Licenciatura')!!}
			</div>
			<div class="input-field col m6 s6">
				<i class="material-icons prefix">school</i>
				{!!Form::text('maestria', null, ['id' => 'maestria'])!!}
				{!!Form::label('maestria', 'Maestría')!!}
			</div>
		</div>
		<div class="row">
			<div class="input-field col m6 s6">
				<i class="material-icons prefix">library_books</i>
				{!!Form::number('experiencia_docente', null, ['id' => 'experiencia_docente'])!!}
				{!!Form::label('experiencia_docente', 'Experiencia docente')!!}
			</div>
			<div class="input-field col m6 s6">
				<i class="material-icons prefix">work</i>
				{!!Form::number('experiencia_profesional', null, ['id' => 'experiencia_profesional'])!!}
				{!!Form::label('experiencia_profesional', 'Esperiencia Profesional')!!}
			</div>
		</div>
		{!!Form::close()!!}
@endif
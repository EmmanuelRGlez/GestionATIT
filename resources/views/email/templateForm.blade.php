<!DOCTYPE html>
<html>
<head>
	<title>
		Index - Encuesta
	</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<style type="text/css">
		.blue {
		  background-color: #1994F9 !important;
		}

		.blue-text {
		  color: #2196F3 !important;
		}

		.grey {
		  background-color: #95989A !important;
		}

		.grey-text {
		  color: #95989A !important;
		}

		html {
		  font-family: sans-serif;
		  /* 1 */
		  -ms-text-size-adjust: 100%;
		  /* 2 */
		  -webkit-text-size-adjust: 100%;
		  /* 2 */
		}

		/**
		 * Remove default margin.
		 */
		body {
		  margin: 0;
		}


		.clearfix {
		  clear: both;
		}

		.z-depth-0 {
		  -webkit-box-shadow: none !important;
		          box-shadow: none !important;
		}

		.z-depth-1, nav, .card-panel, .card, .toast, .btn, .btn-large, .btn-floating, .dropdown-content, .collapsible, .side-nav {
		  -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
		          box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
		}

		.z-depth-1-half, .btn:hover, .btn-large:hover, .btn-floating:hover {
		  -webkit-box-shadow: 0 3px 3px 0 rgba(0, 0, 0, 0.14), 0 1px 7px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -1px rgba(0, 0, 0, 0.2);
		          box-shadow: 0 3px 3px 0 rgba(0, 0, 0, 0.14), 0 1px 7px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -1px rgba(0, 0, 0, 0.2);
		}

		.z-depth-2 {
		  -webkit-box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
		          box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
		}

		.z-depth-3 {
		  -webkit-box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.14), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 3px 5px -1px rgba(0, 0, 0, 0.3);
		          box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.14), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 3px 5px -1px rgba(0, 0, 0, 0.3);
		}

		.z-depth-4, .modal {
		  -webkit-box-shadow: 0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12), 0 5px 5px -3px rgba(0, 0, 0, 0.3);
		          box-shadow: 0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12), 0 5px 5px -3px rgba(0, 0, 0, 0.3);
		}

		.z-depth-5 {
		  -webkit-box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.3);
		          box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.3);
		}


		.container {
		  margin: 0 auto;
		  max-width: 1280px;
		  width: 90%;
		}

		@media only screen and (min-width: 601px) {
		  .container {
		    width: 85%;
		  }
		}

		@media only screen and (min-width: 993px) {
		  .container {
		    width: 70%;
		  }
		}

		.container .row {
		  margin-left: -0.75rem;
		  margin-right: -0.75rem;
		}

		.section {
		  padding-top: 1rem;
		  padding-bottom: 1rem;
		}

		.section.no-pad {
		  padding: 0;
		}

		.section.no-pad-bot {
		  padding-bottom: 0;
		}

		.section.no-pad-top {
		  padding-top: 0;
		}

		.row {
		  margin-left: auto;
		  margin-right: auto;
		  margin-bottom: 0px;
		}

		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		}

		.row .col {
		  float: left;
		  -webkit-box-sizing: border-box;
		          box-sizing: border-box;
		  padding: 0 0.75rem;
		  min-height: 1px;
		}

		.row .col[class*="push-"], .row .col[class*="pull-"] {
		  position: relative;
		}

		.row .col.s1 {
		  width: 8.3333333333%;
		  margin-left: auto;
		  left: auto;
		  right: auto;
		}

		.row .col.s2 {
		  width: 16.6666666667%;
		  margin-left: auto;
		  left: auto;
		  right: auto;
		}

		.row .col.s3 {
		  width: 25%;
		  margin-left: auto;
		  left: auto;
		  right: auto;
		}

		.row .col.s4 {
		  width: 33.3333333333%;
		  margin-left: auto;
		  left: auto;
		  right: auto;
		}

		.row .col.s5 {
		  width: 41.6666666667%;
		  margin-left: auto;
		  left: auto;
		  right: auto;
		}

		.row .col.s6 {
		  width: 50%;
		  margin-left: auto;
		  left: auto;
		  right: auto;
		}

		.row .col.s7 {
		  width: 58.3333333333%;
		  margin-left: auto;
		  left: auto;
		  right: auto;
		}

		.row .col.s8 {
		  width: 66.6666666667%;
		  margin-left: auto;
		  left: auto;
		  right: auto;
		}

		.row .col.s9 {
		  width: 75%;
		  margin-left: auto;
		  left: auto;
		  right: auto;
		}

		.row .col.s10 {
		  width: 83.3333333333%;
		  margin-left: auto;
		  left: auto;
		  right: auto;
		}

		.row .col.s11 {
		  width: 91.6666666667%;
		  margin-left: auto;
		  left: auto;
		  right: auto;
		}

		.row .col.s12 {
		  width: 100%;
		  margin-left: auto;
		  left: auto;
		  right: auto;
		}

		.row .col.offset-s1 {
		  margin-left: 8.3333333333%;
		}

		.row .col.pull-s1 {
		  right: 8.3333333333%;
		}

		.row .col.push-s1 {
		  left: 8.3333333333%;
		}

		.row .col.offset-s2 {
		  margin-left: 16.6666666667%;
		}

		.row .col.pull-s2 {
		  right: 16.6666666667%;
		}

		.row .col.push-s2 {
		  left: 16.6666666667%;
		}

		.row .col.offset-s3 {
		  margin-left: 25%;
		}

		.row .col.pull-s3 {
		  right: 25%;
		}

		.row .col.push-s3 {
		  left: 25%;
		}

		.row .col.offset-s4 {
		  margin-left: 33.3333333333%;
		}

		.row .col.pull-s4 {
		  right: 33.3333333333%;
		}

		.row .col.push-s4 {
		  left: 33.3333333333%;
		}

		.row .col.offset-s5 {
		  margin-left: 41.6666666667%;
		}

		.row .col.pull-s5 {
		  right: 41.6666666667%;
		}

		.row .col.push-s5 {
		  left: 41.6666666667%;
		}

		.row .col.offset-s6 {
		  margin-left: 50%;
		}

		.row .col.pull-s6 {
		  right: 50%;
		}

		.row .col.push-s6 {
		  left: 50%;
		}

		.row .col.offset-s7 {
		  margin-left: 58.3333333333%;
		}

		.row .col.pull-s7 {
		  right: 58.3333333333%;
		}

		.row .col.push-s7 {
		  left: 58.3333333333%;
		}

		.row .col.offset-s8 {
		  margin-left: 66.6666666667%;
		}

		.row .col.pull-s8 {
		  right: 66.6666666667%;
		}

		.row .col.push-s8 {
		  left: 66.6666666667%;
		}

		.row .col.offset-s9 {
		  margin-left: 75%;
		}

		.row .col.pull-s9 {
		  right: 75%;
		}

		.row .col.push-s9 {
		  left: 75%;
		}

		.row .col.offset-s10 {
		  margin-left: 83.3333333333%;
		}

		.row .col.pull-s10 {
		  right: 83.3333333333%;
		}

		.row .col.push-s10 {
		  left: 83.3333333333%;
		}

		.row .col.offset-s11 {
		  margin-left: 91.6666666667%;
		}

		.row .col.pull-s11 {
		  right: 91.6666666667%;
		}

		.row .col.push-s11 {
		  left: 91.6666666667%;
		}

		.row .col.offset-s12 {
		  margin-left: 100%;
		}

		.row .col.pull-s12 {
		  right: 100%;
		}

		.row .col.push-s12 {
		  left: 100%;
		}

		@media only screen and (min-width: 601px) {
		  .row .col.m1 {
		    width: 8.3333333333%;
		    margin-left: auto;
		    left: auto;
		    right: auto;
		  }
		  .row .col.m2 {
		    width: 16.6666666667%;
		    margin-left: auto;
		    left: auto;
		    right: auto;
		  }
		  .row .col.m3 {
		    width: 25%;
		    margin-left: auto;
		    left: auto;
		    right: auto;
		  }
		  .row .col.m4 {
		    width: 33.3333333333%;
		    margin-left: auto;
		    left: auto;
		    right: auto;
		  }
		  .row .col.m5 {
		    width: 41.6666666667%;
		    margin-left: auto;
		    left: auto;
		    right: auto;
		  }
		  .row .col.m6 {
		    width: 50%;
		    margin-left: auto;
		    left: auto;
		    right: auto;
		  }
		  .row .col.m7 {
		    width: 58.3333333333%;
		    margin-left: auto;
		    left: auto;
		    right: auto;
		  }
		  .row .col.m8 {
		    width: 66.6666666667%;
		    margin-left: auto;
		    left: auto;
		    right: auto;
		  }
		  .row .col.m9 {
		    width: 75%;
		    margin-left: auto;
		    left: auto;
		    right: auto;
		  }
		  .row .col.m10 {
		    width: 83.3333333333%;
		    margin-left: auto;
		    left: auto;
		    right: auto;
		  }
		  .row .col.m11 {
		    width: 91.6666666667%;
		    margin-left: auto;
		    left: auto;
		    right: auto;
		  }
		  .row .col.m12 {
		    width: 100%;
		    margin-left: auto;
		    left: auto;
		    right: auto;
		  }
		  .row .col.offset-m1 {
		    margin-left: 8.3333333333%;
		  }
		  .row .col.pull-m1 {
		    right: 8.3333333333%;
		  }
		  .row .col.push-m1 {
		    left: 8.3333333333%;
		  }
		  .row .col.offset-m2 {
		    margin-left: 16.6666666667%;
		  }
		  .row .col.pull-m2 {
		    right: 16.6666666667%;
		  }
		  .row .col.push-m2 {
		    left: 16.6666666667%;
		  }
		  .row .col.offset-m3 {
		    margin-left: 25%;
		  }
		  .row .col.pull-m3 {
		    right: 25%;
		  }
		  .row .col.push-m3 {
		    left: 25%;
		  }
		  .row .col.offset-m4 {
		    margin-left: 33.3333333333%;
		  }
		  .row .col.pull-m4 {
		    right: 33.3333333333%;
		  }
		  .row .col.push-m4 {
		    left: 33.3333333333%;
		  }
		  .row .col.offset-m5 {
		    margin-left: 41.6666666667%;
		  }
		  .row .col.pull-m5 {
		    right: 41.6666666667%;
		  }
		  .row .col.push-m5 {
		    left: 41.6666666667%;
		  }
		  .row .col.offset-m6 {
		    margin-left: 50%;
		  }
		  .row .col.pull-m6 {
		    right: 50%;
		  }
		  .row .col.push-m6 {
		    left: 50%;
		  }
		  .row .col.offset-m7 {
		    margin-left: 58.3333333333%;
		  }
		  .row .col.pull-m7 {
		    right: 58.3333333333%;
		  }
		  .row .col.push-m7 {
		    left: 58.3333333333%;
		  }
		  .row .col.offset-m8 {
		    margin-left: 66.6666666667%;
		  }
		  .row .col.pull-m8 {
		    right: 66.6666666667%;
		  }
		  .row .col.push-m8 {
		    left: 66.6666666667%;
		  }
		  .row .col.offset-m9 {
		    margin-left: 75%;
		  }
		  .row .col.pull-m9 {
		    right: 75%;
		  }
		  .row .col.push-m9 {
		    left: 75%;
		  }
		  .row .col.offset-m10 {
		    margin-left: 83.3333333333%;
		  }
		  .row .col.pull-m10 {
		    right: 83.3333333333%;
		  }
		  .row .col.push-m10 {
		    left: 83.3333333333%;
		  }
		  .row .col.offset-m11 {
		    margin-left: 91.6666666667%;
		  }
		  .row .col.pull-m11 {
		    right: 91.6666666667%;
		  }
		  .row .col.push-m11 {
		    left: 91.6666666667%;
		  }
		  .row .col.offset-m12 {
		    margin-left: 100%;
		  }
		  .row .col.pull-m12 {
		    right: 100%;
		  }
		  .row .col.push-m12 {
		    left: 100%;
		  }
		}

		.card-panel {
		  -webkit-transition: -webkit-box-shadow .25s;
		  transition: -webkit-box-shadow .25s;
		  transition: box-shadow .25s;
		  transition: box-shadow .25s, -webkit-box-shadow .25s;
		  padding: 24px;
		  margin: 0.5rem 0 1rem 0;
		  border-radius: 2px;
		  background-color: #fff;
		}

		.card {
		  position: relative;
		  margin: 0.5rem 0 1rem 0;
		  background-color: #fff;
		  -webkit-transition: -webkit-box-shadow .25s;
		  transition: -webkit-box-shadow .25s;
		  transition: box-shadow .25s;
		  transition: box-shadow .25s, -webkit-box-shadow .25s;
		  border-radius: 2px;
		}

		.card .card-title {
		  font-size: 24px;
		  font-weight: 300;
		}

		.card .card-title.activator {
		  cursor: pointer;
		}

		.card.small, .card.medium, .card.large {
		  position: relative;
		}

		.card.small .card-image, .card.medium .card-image, .card.large .card-image {
		  max-height: 60%;
		  overflow: hidden;
		}

		.card.small .card-image + .card-content, .card.medium .card-image + .card-content, .card.large .card-image + .card-content {
		  max-height: 40%;
		}

		.card.small .card-content, .card.medium .card-content, .card.large .card-content {
		  max-height: 100%;
		  overflow: hidden;
		}

		.card.small .card-action, .card.medium .card-action, .card.large .card-action {
		  position: absolute;
		  bottom: 0;
		  left: 0;
		  right: 0;
		}

		.card.small {
		  height: 300px;
		}

		.card.medium {
		  height: 400px;
		}

		.card.large {
		  height: 500px;
		}

		.card.horizontal {
		  display: -webkit-box;
		  display: -webkit-flex;
		  display: -ms-flexbox;
		  display: flex;
		}

		.card.horizontal.small .card-image, .card.horizontal.medium .card-image, .card.horizontal.large .card-image {
		  height: 100%;
		  max-height: none;
		  overflow: visible;
		}

		.card.horizontal.small .card-image img, .card.horizontal.medium .card-image img, .card.horizontal.large .card-image img {
		  height: 100%;
		}

		.card.horizontal .card-image {
		  max-width: 50%;
		}

		.card.horizontal .card-image img {
		  border-radius: 2px 0 0 2px;
		  max-width: 100%;
		  width: auto;
		}

		.card.horizontal .card-stacked {
		  display: -webkit-box;
		  display: -webkit-flex;
		  display: -ms-flexbox;
		  display: flex;
		  -webkit-box-orient: vertical;
		  -webkit-box-direction: normal;
		  -webkit-flex-direction: column;
		      -ms-flex-direction: column;
		          flex-direction: column;
		  -webkit-box-flex: 1;
		  -webkit-flex: 1;
		      -ms-flex: 1;
		          flex: 1;
		  position: relative;
		}

		.card.horizontal .card-stacked .card-content {
		  -webkit-box-flex: 1;
		  -webkit-flex-grow: 1;
		      -ms-flex-positive: 1;
		          flex-grow: 1;
		}

		.card.sticky-action .card-action {
		  z-index: 2;
		}

		.card.sticky-action .card-reveal {
		  z-index: 1;
		  padding-bottom: 64px;
		}

		.card .card-image {
		  position: relative;
		}

		.card .card-image img {
		  display: block;
		  border-radius: 2px 2px 0 0;
		  position: relative;
		  left: 0;
		  right: 0;
		  top: 0;
		  bottom: 0;
		  width: 100%;
		}

		.card .card-image .card-title {
		  color: #fff;
		  position: absolute;
		  bottom: 0;
		  left: 0;
		  max-width: 100%;
		  padding: 24px;
		}

		.card .card-content {
		  padding: 15px;
		  border-radius: 0 0 2px 2px;
		}

		.card .card-content p {
		  margin: 0;
		  color: inherit;
		}

		.card .card-content .card-title {
		  display: block;
		  line-height: 32px;
		  margin-bottom: 8px;
		}

		.card .card-content .card-title i {
		  line-height: 32px;
		}

		.card .card-action {
		  position: relative;
		  background-color: inherit;
		  border-top: 1px solid rgba(160, 160, 160, 0.2);
		  padding: 16px 24px;
		}

		.card .card-action:last-child {
		  border-radius: 0 0 2px 2px;
		}

		.card .card-action a:not(.btn):not(.btn-large):not(.btn-large):not(.btn-floating) {
		  color: #ffab40;
		  margin-right: 24px;
		  -webkit-transition: color .3s ease;
		  transition: color .3s ease;
		  text-transform: uppercase;
		}

		.card .card-action a:not(.btn):not(.btn-large):not(.btn-large):not(.btn-floating):hover {
		  color: #ffd8a6;
		}

		/*.card .card-reveal {
		  padding: 24px;
		  position: absolute;
		  background-color: #fff;
		  width: 100%;
		  overflow-y: auto;
		  left: 0;
		  top: 100%;
		  height: 100%;
		  z-index: 3;
		  display: none;
		}*/

		.card .card-reveal .card-title {
		  cursor: pointer;
		  display: block;
		}


		.btn, .btn-large,
		.btn-flat, .btn-cancelar {
		  border: none;
		  border-radius: 2px;
		  display: inline-block;
		  height: 36px;
		  line-height: 36px;
		  padding: 0 2rem;
		  margin-right: 12px;
		  text-transform: uppercase;
		  vertical-align: middle;
		  -webkit-tap-highlight-color: transparent;
		}

		.btn, .btn-large,
		.btn-floating,
		.btn-large,
		.btn-flat,
		.btn-cancelar {
		  font-size: 1rem;
		  outline: 0;
		}

		.btn i, .btn-large i,
		.btn-floating i,
		.btn-large i,
		.btn-flat i {
		  font-size: 1.3rem;
		  line-height: inherit;
		}

		.btn:focus, .btn-large:focus,
		.btn-floating:focus {
		  background-color: #1565C0;
		}

		.btn, .btn-large {
		  text-decoration: none;
		  color: #fff;
		  background-color: #1994F9;
		  text-align: center;
		  letter-spacing: .5px;
		  -webkit-transition: .2s ease-out;
		  transition: .2s ease-out;
		  cursor: pointer;
		}

		.btn:hover, .btn-large:hover {
		  background-color: #0277BD;
		}

		.btn-flat {
		  -webkit-box-shadow: none;
		          box-shadow: none;
		  background-color: transparent;
		  color: #343434;
		  cursor: pointer;
		  -webkit-transition: background-color .2s;
		  transition: background-color .2s;
		}

		.btn-flat:focus, .btn-flat:hover {
		  -webkit-box-shadow: none;
		          box-shadow: none;
		}

		.btn-flat:focus {
		  background-color: rgba(0, 0, 0, 0.1);
		}

		.waves-effect {
		  position: relative;
		  cursor: pointer;
		  display: inline-block;
		  overflow: hidden;
		  -webkit-user-select: none;
		     -moz-user-select: none;
		      -ms-user-select: none;
		          user-select: none;
		  -webkit-tap-highlight-color: transparent;
		  vertical-align: middle;
		  z-index: 1;
		  -webkit-transition: .3s ease-out;
		  transition: .3s ease-out;
		}


	</style>
</head>
<body style="font-family: 'Roboto', sans-serif;">
	<div class="content">
		<dic class="section">
			<div class="row" align="center">
				<div class="col s10 offset-s1 m6 offset-m3">
					<div class="card" style="padding: 25px; border-top: 4px solid #e77817;">
						<div class="card-content">
							<img src="{{$message->embed(public_path().'/upslp_logo.png')}}" width="40%">
						</div>
						<div class="card-content" align="center">
							<p>
								<b>Encuesta de disponibilidad de horario</b>
							</p>
						</div>
						<div class="card-content">
							<div class="row" align="center">
								<div style="margin-bottom: 30px;">
									<p class="grey-text">
										Responde a la siguiente encuesta para poder registrar estadísticas para el periodo de <b>{{$periodo->nombre}}</b> sobre los horarios y las materias a los que puede asistir a impartir clases.
									</p>
								</div>
								<div style="margin-bottom: 30px;">
									<a href="{{url('/encuestas/responder/'.$token)}}"><button class="waves-effect waves-light btn" type="button" style="margin-right: 0;"><b>CONTINUAR</b></button></a>
								</div>
								<div>
									<p class="grey-text" style="font-size: 12px;">
										*Para cualquier duda o aclaración, contacte con el administrador.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>					
	</div>
</body>
</html>

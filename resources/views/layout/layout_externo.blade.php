<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
	{{-- Etiqueta para herencia al head-meta de la página --}}
	@yield('meta')
	<title>ATIT - @yield('title')</title>

	<!-- CSS  -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	{{-- <link href="/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/> --}}
	{{-- Etiqueta para herencia al head-link de la página --}}
	@yield('link')
</head>
<body>
	{{-- Etiqueta para herencia al cuerpo al inicio de la página --}}
	@yield('body-begin')

	{{-- <div class="section" id="index-banner">
		<div class="container">
			<h1 class="header center orange-text">Starter Template</h1>
			<div class="row center">
				<h5 class="header col s12 light">A modern responsive front-end framework based on Material Design</h5>
			</div>
		</div>
	</div> --}}


	<div class="container">
		<div class="section">
			{{-- Etiqueta para herencia al cuerpo al contenido de la página --}}
			@yield('body')
		</div>
	</div>

	{{-- Etiqueta para herencia al cuerpo al final de la página --}}
	@yield('body-end')

	{{--
	<footer class="page-footer orange">
		<div class="container">
			<div class="row">
				<div class="col l6 s12">
					<h5 class="white-text">Company Bio</h5>
					<p class="grey-text text-lighten-4">We are a team of college students working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated.</p>


				</div>
				<div class="col l3 s12">
					<h5 class="white-text">Settings</h5>
					<ul>
						<li><a class="white-text" href="#!">Link 1</a></li>
						<li><a class="white-text" href="#!">Link 2</a></li>
						<li><a class="white-text" href="#!">Link 3</a></li>
						<li><a class="white-text" href="#!">Link 4</a></li>
					</ul>
				</div>
				<div class="col l3 s12">
					<h5 class="white-text">Connect</h5>
					<ul>
						<li><a class="white-text" href="#!">Link 1</a></li>
						<li><a class="white-text" href="#!">Link 2</a></li>
						<li><a class="white-text" href="#!">Link 3</a></li>
						<li><a class="white-text" href="#!">Link 4</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="footer-copyright">
			<div class="container">
			Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
			</div>
		</div>
	--}}
	</footer>

	<!--  Scripts-->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="/js/materialize.js"></script>
	
	{{-- Etiqueta para herencia a los script de la página --}}
	@yield('script')
	<script type="text/javascript" charset="utf-8" async defer>
		$('select').formSelect();
	</script>
	</body>
</html>
